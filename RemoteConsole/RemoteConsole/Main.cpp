#include "RemoteConsole.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <tchar.h>
#include <iostream>
#include <string>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
using namespace sys;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const CHAR DEFAULT_PIPE_NAME[] = "\\\\.\\pipe\\server";
static const std::string EXIT_CONSOLE_CMD = "$exit"; 
static const std::string HELP_CONSOLE_CMD = "$help"; 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void PrintHelp();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void _tmain(int argc, _TCHAR* argv[])
{	
	std::string cmd, ans, pipeName = DEFAULT_PIPE_NAME;

	RemoteConsole* rConsole = new RemoteConsole;

	std::cout << "Remote Console v1.0. All right reserved.\n\n";

	do
	{
		if (!rConsole->Connect(pipeName.c_str()))
		{
			std::cout <<
				"Please enter pipe name (for example, '\\\\DnsName\\Pipe\\ServerPipeName') or $exit\n";
			std::cout << "\n>> ";
			getline(std::cin, cmd);
			if (cmd != EXIT_CONSOLE_CMD && rConsole->Connect(cmd.c_str()))
				pipeName = cmd;
			else std::cout << "\n";
		}

		if (rConsole->IsConnected())
			std::cout << "Connected to pipe '" << pipeName << "'. Help command - '$help'\n";
		
		while (rConsole->IsConnected() && cmd != EXIT_CONSOLE_CMD)
		{
			std::cout << "\n>>> ";
			getline(std::cin, cmd);
			if (cmd != EXIT_CONSOLE_CMD)
			{
				if (cmd != HELP_CONSOLE_CMD)
				{
					if (rConsole->Execute(cmd.c_str(), ans))
						std::cout << "Answer from Server: \n\n" << ans << "\n";
				}
				else PrintHelp();
			}
		}
	} 
	while (cmd != EXIT_CONSOLE_CMD);

	delete rConsole;
}
void PrintHelp()
{
	std::cout << "\n------------------------------\n";
	std::cout << "\nApplication commands:";
	std::cout << "\n\t$exit - shutdown application";
	std::cout << "\nRemote commands:";
	std::cout << "\n\tstart - start accept and response server objects";
	std::cout << "\n\tstop - stop accept and responce server objects";
	std::cout << "\n\tstat, statistics - statistics information";
	std::cout << "\n\twait - prohibition TCP-connections while work connected clients";
	std::cout << "\n\tshutdown - equal wait + stop";
	std::cout << "\n\texit - stop server and end server process";
	std::cout << "\n\n------------------------------\n";
}