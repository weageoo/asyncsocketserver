#pragma once
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <Windows.h>
#include <string>

namespace sys
{
	class RemoteConsole
	{
	private:
		HANDLE m_hNp;         // ��������� ���������� ������������ ������
		BOOL   m_isConnected; // ����������� �� ������ � �������?
	public:
		RemoteConsole();
	   ~RemoteConsole();
		BOOL Connect(LPCSTR pipeName);  // �������������� � �������
		BOOL Connect(                   // �������������� � �������
			LPCSTR pipeName,            //     ��� ������
			DWORD waitTimeMsec);        //     ����� �������� ���������� ���������� ������
		BOOL IsConnected() const;       // ����������� �� ������ � �������?
		BOOL Execute(LPCSTR command);   // ������� ������� (��� �������������)
		BOOL Execute(                   // ������� �������
			LPCSTR command,             //	   ���������� ������ ������� 
			std::string& answer);       //	   ����� �� �������
		VOID Disconnect();              // ������������� �� ���������� ������
	};
}