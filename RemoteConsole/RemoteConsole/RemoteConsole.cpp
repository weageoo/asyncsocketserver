#include "RemoteConsole.h"

namespace sys
{
	RemoteConsole::RemoteConsole()
	{
		m_hNp = INVALID_HANDLE_VALUE;
		m_isConnected = FALSE;
	}

	RemoteConsole::~RemoteConsole()
	{
		this->Disconnect();
	}

	BOOL RemoteConsole::Connect(LPCSTR pipeName, DWORD waitTimeMsec)
	{
		if (::WaitNamedPipe(pipeName, waitTimeMsec
			) == 0)
			return FALSE;

		m_hNp = ::CreateFile(
			pipeName,                     // ��� ������
			GENERIC_READ | GENERIC_WRITE, // ����� ������� - ������ + ������ ������ � �����
			0,                            // ��� ������ �������
			NULL,                         // ������������ �� ���������
			OPEN_EXISTING,                // ���������� ������, ���� ����� �� ����������
			FILE_ATTRIBUTE_NORMAL,        // �������� � ����� �����
			NULL                          // �� ����������� ��� ������ � ������������ ��������
			);
		if (m_hNp == INVALID_HANDLE_VALUE)
			return FALSE;

		return (m_isConnected = TRUE);
	}

	BOOL RemoteConsole::Connect(LPCSTR pipeName)
	{
		return Connect(pipeName, INFINITE);
	}

	BOOL RemoteConsole::IsConnected() const
	{
		return m_isConnected;
	}

	BOOL RemoteConsole::Execute(LPCSTR command)
	{
		BOOL res = FALSE;

		if (IsConnected())
		{
			DWORD bytesWritten = 0; // ���� ��������

			__try
			{
				res = WriteFile(m_hNp, command, ::strlen(command) + 1, &bytesWritten, NULL);
			}
			__finally 
			{ 
				if (!res) 
					Disconnect(); 
			}
		}

		return res;
	}

	BOOL RemoteConsole::Execute(LPCSTR command, std::string& answer)
	{
		BOOL res = TRUE;
		CHAR buffer[1024] = "";
		DWORD dwBytesRead = 0;

		if (res = Execute(command))
		{
			__try 
			{
				res = ReadFile(m_hNp, buffer, sizeof(buffer), &dwBytesRead, NULL);
				if (res)
					answer = buffer;
			}
			__finally 
			{ 
				if (!res)
					Disconnect();
			}
		}
		return res;
	}

	VOID RemoteConsole::Disconnect()
	{
		if (IsConnected())
		{
			::DisconnectNamedPipe(m_hNp);
			::CloseHandle(m_hNp);
			m_hNp = INVALID_HANDLE_VALUE;
			m_isConnected = false;
		}
	}
}