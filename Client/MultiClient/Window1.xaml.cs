﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Threading;

namespace MultiClient
{
    public partial class Window1 : Window
    {
        private string m_servName = Properties.Settings.Default.ServerName;
        private string m_servCall = Properties.Settings.Default.ServerCallsing;
        private ushort m_servPort = Properties.Settings.Default.ServerPort;
        private double m_updateFreq = Properties.Settings.Default.UpdateFreq;

        private ClientCOMLib.ClientClass m_client = new ClientCOMLib.ClientClass();
        private System.Timers.Timer m_timer = new System.Timers.Timer();
        private System.Timers.Timer m_servTimer = new System.Timers.Timer();

        public Window1()
        {
            InitializeComponent();
        }

        private bool ConnectToServer()
        {
            return ( m_client.TryConnect(m_servName, m_servCall, m_servPort) > 0 );
        }

        private void Disconnect()
        {
            m_client.Disconnect();
        }

        private void ShowError(string err)
        { 
            m_servTimer.Stop();
            c_nonStopRand.IsChecked = false;
            c_nonStopTime.IsChecked = false;
            m_servTimer.Start();

            c_textConsole.Text = err;
            c_popupConsole.IsOpen = true;
        }

        private void RandServiceUpdate()
        {
            try
            {
                if (m_client.IsConnected() > 0)
                {
                    int randVal = m_client.CallRand((int)c_minSlider.Value, (int)c_maxSlider.Value);
                    c_randResult.Text = String.Format(
                        "Результат выполнения rand-сервиса на промежутке\r\nот {0} до {1}\r\n>> {2}",
                        (int)c_minSlider.Value, (int)c_maxSlider.Value, randVal);
                }
            }
            catch (COMException ex)
            {
                ShowError(ex.Message);
            }
        }

        private void TimeServiceUpdate()
        {
            try
            {
                if (m_client.IsConnected() > 0)
                {
                    ClientCOMLib.DateTime t =
                        m_client.CallTime(
                        (int)((TimeZoneInfo)c_timeZone.SelectedItem).BaseUtcOffset.TotalMinutes);
                    c_timeResult.Text = String.Format(
                    "Результат выполнения time-сервиса для указанного часового пояса\r\n>> {0}",
                    (new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, t.Second).ToString()));
                }
            }
            catch (COMException ex)
            {
                ShowError(ex.Message);
            }
        }

        private void PostServiceUpdate()
        {
            try
            {
                if (m_client.IsConnected() > 0)
                {
                    string body = c_mailBody.Text.Replace("\r\n", "<br />");
                    string res = m_client.CallPost(c_mailSender.Text, c_mailSubject.Text, body);
                    c_postResult.Text = String.Format(
                        (res[0] == '+') ?
                        "Сообщение отправлено успешно. \r\nСервер прислал:\r\n {0}" :
                        "Сообщение не было доставлено. \r\nОшибка:\r\n {0}",
                        res);
                }
            }
            catch (COMException ex)
            {
                ShowError(ex.Message);
            }
        }

        private void UpdateTimersIntervals()
        {
            double connInterval = 1;
            if (double.TryParse(c_connInterval.Text, out connInterval) && connInterval >= 1)
                m_timer.Interval = 1000 * connInterval;

            m_servTimer.Interval = 10 * m_updateFreq;
        }

        private void UpdateConnState()
        {
            if (m_client.IsConnected() == 0/* && !ConnectToServer()*/)
            {
                c_connState.Content = "отключено";
                c_connState.ToolTip = "Жёлкни, чтобы подключится.";
            }
            else
            {
                c_connState.Content = "подключено";
                c_connState.ToolTip = "Жёлкни, чтобы отключится.";
            }
        }

        private void FillTimeZoneList()
        {
            foreach(var zone in TimeZoneInfo.GetSystemTimeZones()) 
            {
                c_timeZone.Items.Add(zone);
                if (zone.DisplayName == TimeZoneInfo.Local.DisplayName)
                    c_timeZone.SelectedItem = zone;
            }
        }

        private void c_minSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            RandServiceUpdate();
        }

        private void c_maxSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            RandServiceUpdate();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillTimeZoneList();
            m_timer.Elapsed += m_timer_Elapsed;
            UpdateTimersIntervals();
            m_timer.Start();
            m_servTimer.Start();
            ConnectToServer();
            UpdateConnState();
        }
        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            Disconnect();
        }

        private void m_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.SystemIdle, (ThreadStart)(() => UpdateConnState()));
        }

        private void elapsed_Rand(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (ThreadStart)(() => RandServiceUpdate()));
        }
        private void elapsed_Time(object sender, System.Timers.ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Background, (ThreadStart)(() => TimeServiceUpdate()));
        }

        private void c_connInterval_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateTimersIntervals();
        }
        private void c_popupConsole_MouseDown(object sender, MouseButtonEventArgs e)
        {
            c_popupConsole.IsOpen = false;
        }

        private void c_defaultSettingsBtn_Click(object sender, RoutedEventArgs e)
        {
            c_servName.Text = Properties.Settings.Default.ServerName;
            c_servPort.Text = Properties.Settings.Default.ServerPort.ToString();
            c_servCall.Text = Properties.Settings.Default.ServerCallsing;
            c_updateFreq.Text = Properties.Settings.Default.UpdateFreq.ToString();
        }
        private void c_servName_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_servName = c_servName.Text;
        }
        private void c_servCall_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_servCall = c_servCall.Text;
        }
        private void c_servPort_TextChanged(object sender, TextChangedEventArgs e)
        {
            ushort servPort = m_servPort;
            ushort.TryParse(c_servPort.Text, out servPort);
            m_servPort = servPort;
        }
        private void c_updateFreq_TextChanged(object sender, TextChangedEventArgs e)
        {
            double updateFreq = m_updateFreq;
            double.TryParse(c_updateFreq.Text, out updateFreq);
            if (updateFreq >= 1)
                m_updateFreq = updateFreq;
            m_servTimer.Stop();
            m_servTimer.Interval = 10 * m_updateFreq;
            m_servTimer.Start();
        }

        private void c_connState_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (c_connState.Content.ToString() == "отключено" && m_client.IsConnected() == 0)
                ConnectToServer();
            else if (c_connState.Content.ToString() == "подключено")
                m_client.Disconnect();
            UpdateConnState();
        }

        private void c_nonStopRand_Checked(object sender, RoutedEventArgs e)
        {
            m_servTimer.Elapsed += elapsed_Rand;
        }
        private void c_nonStopRand_Unchecked(object sender, RoutedEventArgs e)
        {
            m_servTimer.Elapsed -= elapsed_Rand;
        }
        private void c_nonStopTime_Checked(object sender, RoutedEventArgs e)
        {
            m_servTimer.Elapsed += elapsed_Time;
        }
        private void c_nonStopTime_Unchecked(object sender, RoutedEventArgs e)
        {
            m_servTimer.Elapsed -= elapsed_Time;
        }

        private void c_randUpdate_Click(object sender, RoutedEventArgs e)
        {
            RandServiceUpdate();
        }
        private void c_timeUpdate_Click(object sender, RoutedEventArgs e)
        {
            TimeServiceUpdate();
        }
        private void c_postUpdate_Click(object sender, RoutedEventArgs e)
        {
            PostServiceUpdate();
        }

        private void c_timeZone_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TimeServiceUpdate();
        }
       
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Normal;
            this.SizeToContent = SizeToContent.WidthAndHeight;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
        }
    }
}
