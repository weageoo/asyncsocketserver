#include "stdafx.h"
#include "Client.h"

#define BUFLEN               2056
#define BROADCAST_MSGS_COUNT 5    // ���-�� ���������-��������, ���������� ��� �������������
#define WAIT_TIME_MSEC       300  // ����� �������� ������ �� ����������������� ������

STDMETHODIMP CClient::Connect(BSTR servname, BSTR callsing, USHORT servport)
{
	return x_FindAndConnect(servname, callsing, servport);
}

STDMETHODIMP CClient::TryConnect(BSTR servname, BSTR callsing, USHORT servport, PBOOL connected)
{
	*connected = (x_FindAndConnect(servname, callsing, servport) == NOERROR);
	return S_OK;
}

STDMETHODIMP CClient::IsConnected(PBOOL connected)
{
	*connected = FALSE;

	if (m_sock != INVALID_SOCKET)
	{
		BSTR ans;
		if (SUCCEEDED(CallEcho(L".", &ans)))
			*connected = TRUE;
		else Disconnect();
	}

	return S_OK;
}

STDMETHODIMP CClient::CallEcho(BSTR msg, LPBSTR ans)
{
	HRESULT hr = S_OK;
	CMarkup xml;
	std::string ansDoc;

	xml.AddElem(XMLTAG_REQUEST);
	xml.AddChildElem(XMLTAG_SERVICE, ECHO_SERV_NAME);
	xml.AddChildElem(XMLTAG_PARAMS);
	xml.IntoElem();
	xml.AddChildElem(XMLTAG_MSG, x_WcharToString(msg));

	if ( SUCCEEDED(hr = x_SendRecv(xml.GetDoc(), ansDoc)) )
	{
		xml.SetDoc(ansDoc);
		if (xml.FindChildElem(XMLTAG_RESULT))
		{
			xml.IntoElem();
			if ( xml.FindChildElem(XMLTAG_MSG) )
				*ans = x_AllocString(xml.GetChildData());
		}
		else if ( xml.FindChildElem(XMLTAG_ERRORS) )
		{
			hr = E_FAIL;
			//xml.IntoElem();
			//if /*while*/ (xml.FindChildElem(XMLTAG_ERROR))
			//	*ans = SysAllocString(x_CharToWchar(xml.GetChildData().c_str()));
		}
	}

	return hr;
}

STDMETHODIMP CClient::CallRand(INT minValue, INT maxValue, PINT genValue)
{
	HRESULT hr = S_OK;
	CMarkup xml;
	std::string ansDoc;

	xml.AddElem(XMLTAG_REQUEST);
	xml.AddChildElem(XMLTAG_SERVICE, RAND_SERV_NAME);
	xml.AddChildElem(XMLTAG_PARAMS);
	xml.IntoElem();
	xml.AddChildElem(XMLTAG_MIN, minValue);
	xml.AddChildElem(XMLTAG_MAX, maxValue);

	if ( SUCCEEDED(hr = x_SendRecv(xml.GetDoc(), ansDoc)) )
	{
		xml.SetDoc(ansDoc);
		if ( xml.FindChildElem(XMLTAG_RESULT) )
		{
			xml.IntoElem();
			if ( xml.FindChildElem(XMLTAG_NUM) )
				*genValue = ::atoi(xml.GetChildData().c_str());
		}
		else if ( xml.FindChildElem(XMLTAG_ERRORS) )
		{
			hr = E_FAIL;
		}
	}

	return hr;
}

STDMETHODIMP CClient::CallTime(INT timezone, DateTime* dateTime)
{
	HRESULT hr = S_OK;
	CMarkup xml;
	std::string ansDoc;

	xml.AddElem(XMLTAG_REQUEST);
	xml.AddChildElem(XMLTAG_SERVICE, TIME_SERV_NAME);
	xml.AddChildElem(XMLTAG_PARAMS);
	xml.IntoElem();
	xml.AddChildElem(XMLTAG_TIMEZONE, timezone);

	if ( SUCCEEDED(hr = x_SendRecv(xml.GetDoc(), ansDoc)) )
	{
		xml.SetDoc(ansDoc);
		if ( xml.FindChildElem(XMLTAG_RESULT) )
		{
			xml.IntoElem();
			if ( xml.FindChildElem(XMLTAG_YEAR) )
				dateTime->Year = ::atoi(xml.GetChildData().c_str());
			if ( xml.FindChildElem(XMLTAG_MONTH) )
				dateTime->Month = ::atoi(xml.GetChildData().c_str());		
			if ( xml.FindChildElem(XMLTAG_DAY_OF_WEEK) )
				dateTime->DayOfWeek = ::atoi(xml.GetChildData().c_str());
			if ( xml.FindChildElem(XMLTAG_DAY) )
				dateTime->Day = ::atoi(xml.GetChildData().c_str());
			if ( xml.FindChildElem(XMLTAG_HOUR) )
				dateTime->Hour = ::atoi(xml.GetChildData().c_str());
			if ( xml.FindChildElem(XMLTAG_MINUTE) )
				dateTime->Minute = ::atoi(xml.GetChildData().c_str());
			if ( xml.FindChildElem(XMLTAG_SECOND) )
				dateTime->Second = ::atoi(xml.GetChildData().c_str());
			if ( xml.FindChildElem(XMLTAG_TIMEZONE) )
				dateTime->TimeZone = ::atoi(xml.GetChildData().c_str());
		}
		else if ( xml.FindChildElem(XMLTAG_ERRORS) )
		{
			hr = E_FAIL;
		}
	}

	return hr;
}

STDMETHODIMP CClient::CallPost(BSTR mailer, BSTR subject, BSTR body, LPBSTR info)
{
	HRESULT hr = S_OK;
	CMarkup xml;
	std::string ansDoc;

	xml.AddElem(XMLTAG_REQUEST);
	xml.AddChildElem( XMLTAG_SERVICE, POST_SERV_NAME );
	xml.AddChildElem( XMLTAG_PARAMS );
	xml.IntoElem();
	xml.AddChildElem( XMLTAG_MAILER, x_WcharToString(mailer) );
	xml.AddChildElem( XMLTAG_SUBJECT, x_WcharToString(subject) );
	xml.AddChildElem( XMLTAG_MSG, x_WcharToString(body) );

	if ( SUCCEEDED(hr = x_SendRecv(xml.GetDoc(), ansDoc)) )
	{
		xml.SetDoc(ansDoc);
		if ( xml.FindChildElem(XMLTAG_RESULT) )
		{
			xml.IntoElem();
			if ( xml.FindChildElem(XMLTAG_MSG) )
				*info = x_AllocString(xml.GetChildData());
		}
		else if ( xml.FindChildElem(XMLTAG_ERRORS) )
		{
			xml.IntoElem();
			while (xml.FindChildElem(XMLTAG_ERROR))
				*info = x_AllocString(xml.GetChildData());
		}
	}

	return hr;
}

STDMETHODIMP CClient::Disconnect()
{
	if (m_sock != INVALID_SOCKET)
	{
		x_SendQuitRq();
		::closesocket(m_sock);
		m_sock = INVALID_SOCKET;
	}
	return S_OK;
}

std::string CClient::x_WcharToString(const wchar_t* str)
{
	std::string finalStr;

	size_t sz = ::wcslen(str) + 1;
	
	char* cStr = new char[sz];
	::wcstombs_s(&sz, cStr, sz, str, _TRUNCATE);
	finalStr = cStr;
	delete cStr;

	return finalStr;
}
BSTR CClient::x_AllocString(std::string str)
{
	BSTR allocStr = NULL;
	size_t sz = str.length() * sizeof(wchar_t);
	wchar_t* wStr = new wchar_t[sz];

	::mbstowcs_s(&sz, wStr, sz, str.c_str(), _TRUNCATE);
	allocStr = ::SysAllocString(wStr);
	
	delete wStr;

	return allocStr;
}
bool CClient::x_FindByName(std::string servname)
{
	PSTR ip;
	PHOSTENT pHostent;

	if ((pHostent = ::gethostbyname(servname.c_str())) == NULL)
		return false;

	// ������ ������ ����� ���������� ���. �������� ������ IP �� ������.
	ip = ::inet_ntoa(*(struct in_addr *)*pHostent->h_addr_list);
	m_sParams.sin_addr.s_addr = ::inet_addr(ip);

	return true;
}
bool CClient::x_FindBroadcast(std::string callsing)
{
	bool isFind = false;
	SOCKET udpSock = INVALID_SOCKET;
	char iBuf[256] = "";
	FD_SET readSet;
	static timeval tmv = { 0, WAIT_TIME_MSEC * 1000 };

	if ( (udpSock = ::socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET )
		return false;

	u_long ulBlock = 1; // ������������� ����� (�������� ������� FIONBIO)
	if ( ::ioctlsocket(udpSock, FIONBIO, &ulBlock) == SOCKET_ERROR )
		return false;

	int optVal = 1;
	if ( ::setsockopt(udpSock, SOL_SOCKET, SO_BROADCAST, (char*)&(optVal), sizeof(int)) == SOCKET_ERROR )
		return false;

	m_sParams.sin_addr.s_addr = ::htonl(INADDR_BROADCAST);

	for (int i = BROADCAST_MSGS_COUNT, fromLen = sizeof(SOCKADDR); (--i >= 0) && (!isFind); )
	{
		FD_ZERO(&readSet);
		FD_SET(udpSock, &readSet);

		if((::sendto(udpSock, callsing.c_str(), callsing.length(), NULL,
			(PSOCKADDR)&m_sParams, sizeof(SOCKADDR))) != SOCKET_ERROR)
		{
			if (::select(0, &readSet, NULL, NULL, &tmv) != SOCKET_ERROR)
			{
				if (FD_ISSET(udpSock, &readSet))
				{
					if (::recvfrom(udpSock, iBuf, sizeof(iBuf), NULL,
						(PSOCKADDR)&m_sParams, &fromLen) != SOCKET_ERROR)
					{
						isFind = (::strcmp(callsing.c_str(), iBuf) == 0);
					}
				}
			}
		}
	}
	::closesocket(udpSock);

	return isFind;
}
HRESULT CClient::x_SendRecv(std::string msg, std::string &ans)
{
	HRESULT hr = S_OK;
	char iBuf[BUFLEN] = "";
	int iBufLen = 0, // ����������� �������� ����
		oBufLen = 0; // ����������� ������������ ����

	if ((oBufLen = ::send(m_sock, msg.c_str(), msg.length(), NULL)) == SOCKET_ERROR)
		hr = HRESULT_FROM_WIN32(::WSAGetLastError());
	else if ((iBufLen = ::recv(m_sock, iBuf, sizeof(iBuf), NULL)) == SOCKET_ERROR)
		hr = HRESULT_FROM_WIN32(::WSAGetLastError());

	if (hr == S_OK)
		ans = iBuf;

	return hr;
}

HRESULT CClient::x_FindAndConnect(BSTR servname, BSTR callsing, USHORT servport)
{
	if ( (servname == NULL && callsing == NULL) || (servport <= 0) )
		return E_INVALIDARG;

	HRESULT hr = S_OK;
	BOOL connected = FALSE;

	if ( SUCCEEDED( hr = IsConnected(&connected) ) && !connected )
	{
		m_sParams.sin_family = AF_INET;
		m_sParams.sin_port = ::htons(servport);

		if (servname == NULL || !x_FindByName(x_WcharToString(servname).c_str()) || FAILED(x_Connect()))
			if (callsing == NULL || !x_FindBroadcast(x_WcharToString(callsing).c_str()) || FAILED(x_Connect()))
				hr = HRESULT_FROM_WIN32(WSASERVICE_NOT_FOUND);
	}
	return hr;
}

HRESULT CClient::x_Connect()
{
	HRESULT hr = S_OK;

	if ((m_sock = ::socket(AF_INET, SOCK_STREAM, NULL)) == INVALID_SOCKET)
		hr = HRESULT_FROM_WIN32(::WSAGetLastError());
	else if ((::connect(m_sock, (PSOCKADDR)&m_sParams, sizeof(m_sParams))) == SOCKET_ERROR)
	{
		hr = HRESULT_FROM_WIN32(::WSAGetLastError());
		::closesocket(m_sock);
		m_sock = INVALID_SOCKET;
	}

	return hr;
}

void CClient::x_SendQuitRq()
{
	CMarkup xml;
	xml.AddElem(XMLTAG_REQUEST);
	xml.AddChildElem(XMLTAG_SERVICE, QUIT_SERV_NAME);
	::send(m_sock, xml.GetDoc().c_str(), xml.GetDoc().length(), NULL);
}

#undef BUFLEN
#undef BROADCAST_MSGS_COUNT
#undef WAIT_TIME_MSEC