// dllmain.h : Declaration of module class.

class CClientCOMModule : public CAtlDllModuleT< CClientCOMModule >
{
public :
	DECLARE_LIBID(LIBID_ClientCOMLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CLIENTCOM, "{D78A43F6-92B5-4320-A4BF-9ACBF341FBB6}")
};

extern class CClientCOMModule _AtlModule;
