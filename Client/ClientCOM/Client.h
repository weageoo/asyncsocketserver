// Client.h : Declaration of the CClient

#pragma once
#include "resource.h"       // main symbols
#include "Markup.h"
#include <string>
#include <WinSock2.h>
#pragma comment(lib, "WS2_32.lib")

#include "ClientCOM_i.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

///
static const TCHAR XMLTAG_PARAMS[]      = _TEXT("PARAMS");
static const TCHAR XMLTAG_RESPONSE[]    = _TEXT("RESPONSE");
static const TCHAR XMLTAG_REQUEST[]     = _TEXT("REQUEST");
static const TCHAR XMLTAG_SERVICE[]     = _TEXT("SERVICE");
static const TCHAR XMLTAG_RESULT[]      = _TEXT("RESULT");
static const TCHAR XMLTAG_ERRORS[]      = _TEXT("ERRORS");
static const TCHAR XMLTAG_ERROR[]       = _TEXT("ERROR");
static const TCHAR XMLTAG_MAILER[]      = _TEXT("MAILER");
static const TCHAR XMLTAG_SUBJECT[]     = _TEXT("SUBJECT");
static const TCHAR XMLTAG_MSG[]         = _TEXT("MSG");
static const TCHAR XMLTAG_MIN[]         = _TEXT("MIN");
static const TCHAR XMLTAG_MAX[]         = _TEXT("MAX");
static const TCHAR XMLTAG_NUM[]         = _TEXT("NUM");
static const TCHAR XMLTAG_TIMEZONE[]    = _TEXT("TIMEZONE");
static const TCHAR XMLTAG_YEAR[]        = _TEXT("YEAR");
static const TCHAR XMLTAG_MONTH[]       = _TEXT("MONTH");
static const TCHAR XMLTAG_DAY_OF_WEEK[] = _TEXT("DAY_OF_WEEK");
static const TCHAR XMLTAG_DAY[]         = _TEXT("DAY");
static const TCHAR XMLTAG_HOUR[]        = _TEXT("HOUR");
static const TCHAR XMLTAG_MINUTE[]      = _TEXT("MINUTE");
static const TCHAR XMLTAG_SECOND[]      = _TEXT("SECOND");

static const TCHAR ECHO_SERV_NAME[]     = _TEXT("echo");
static const TCHAR RAND_SERV_NAME[]     = _TEXT("rand");
static const TCHAR TIME_SERV_NAME[]     = _TEXT("time");
static const TCHAR POST_SERV_NAME[]     = _TEXT("post");
static const TCHAR QUIT_SERV_NAME[]     = _TEXT("quit");
///

//typedef struct DateTime
//{
//	unsigned short Year;
//	unsigned short Month;
//	unsigned short DayOfWeek;
//	unsigned short Day;
//	unsigned short Hour;
//	unsigned short Minute;
//	unsigned short Second;
//	long TimeZone;
//} DATETIME, PDATETIME;

// CClient

class ATL_NO_VTABLE CClient :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CClient, &CLSID_Client>,
	public IDispatchImpl<IClient, &IID_IClient, &LIBID_ClientCOMLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CClient()
	{
		WSADATA wsaData;

		if (::WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
			throw;

		m_sock = INVALID_SOCKET;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CLIENT)


BEGIN_COM_MAP(CClient)
	COM_INTERFACE_ENTRY(IClient)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
		Disconnect();
		::WSACleanup();
	}

private:
	SOCKET m_sock;
	SOCKADDR_IN m_sParams;

public:
	STDMETHOD(Connect)(
		IN BSTR   servname, // [in] ��� �������
		IN BSTR   callsing, // [in] �������� �������
 		IN USHORT servport  // [in] ����� �����
	);
	STDMETHOD(TryConnect)(
		IN  BSTR   servname,  // [in]  ��� �������
		IN  BSTR   callsing,  // [in]  �������� �������
		IN  USHORT servport,  // [in]  ����� �����
		OUT PBOOL  connected  // [out] ������� �� �����������?
		);
	STDMETHOD(IsConnected)(
		OUT PBOOL connected
	);
	STDMETHOD(CallEcho)(
		IN  BSTR   msg,
		OUT LPBSTR ans
	);
	STDMETHOD(CallRand)(
		IN  INT  minValue,
		IN  INT  maxValue,
		OUT PINT genValue
	);
	STDMETHOD(CallTime)(
		IN  INT timezone,
		OUT DateTime* dateTime
	);
	STDMETHOD(CallPost)(
		IN  BSTR mailer,
		IN  BSTR subject,
		IN  BSTR body,
		OUT LPBSTR info
	);
	STDMETHOD(Disconnect)();

private:
	std::string x_WcharToString(const wchar_t* str);
	BSTR x_AllocString(std::string str);
	bool x_FindByName(std::string servname);
	bool x_FindBroadcast(std::string callsing);
	void x_SendQuitRq();
	HRESULT x_SendRecv(std::string msg, std::string& ans);
	HRESULT x_FindAndConnect(BSTR servname, BSTR callsing, USHORT servport);
	HRESULT x_Connect();
};

OBJECT_ENTRY_AUTO(__uuidof(Client), CClient)
