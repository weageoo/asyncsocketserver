

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Tue Dec 20 14:59:53 2011
 */
/* Compiler settings for ClientCOM.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __ClientCOM_i_h__
#define __ClientCOM_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IClient_FWD_DEFINED__
#define __IClient_FWD_DEFINED__
typedef interface IClient IClient;
#endif 	/* __IClient_FWD_DEFINED__ */


#ifndef __Client_FWD_DEFINED__
#define __Client_FWD_DEFINED__

#ifdef __cplusplus
typedef class Client Client;
#else
typedef struct Client Client;
#endif /* __cplusplus */

#endif 	/* __Client_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_ClientCOM_0000_0000 */
/* [local] */ 

typedef /* [public][public] */ struct __MIDL___MIDL_itf_ClientCOM_0000_0000_0001
    {
    unsigned short Year;
    unsigned short Month;
    unsigned short DayOfWeek;
    unsigned short Day;
    unsigned short Hour;
    unsigned short Minute;
    unsigned short Second;
    int TimeZone;
    } 	DateTime;



extern RPC_IF_HANDLE __MIDL_itf_ClientCOM_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_ClientCOM_0000_0000_v0_0_s_ifspec;

#ifndef __IClient_INTERFACE_DEFINED__
#define __IClient_INTERFACE_DEFINED__

/* interface IClient */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IClient;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("865E4B62-88A5-4484-854A-2EBD577CED15")
    IClient : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Connect( 
            /* [in] */ BSTR serverName,
            /* [in] */ BSTR serverCallsing,
            /* [in] */ unsigned short port) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE TryConnect( 
            /* [in] */ BSTR serverName,
            /* [in] */ BSTR serverCallsing,
            /* [in] */ unsigned short port,
            /* [retval][out] */ BOOL *connected) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IsConnected( 
            /* [retval][out] */ BOOL *connected) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Disconnect( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallEcho( 
            /* [in] */ BSTR msg,
            /* [retval][out] */ BSTR *ans) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallRand( 
            /* [in] */ int minValue,
            /* [in] */ int maxValue,
            /* [retval][out] */ int *genValue) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallTime( 
            /* [in] */ int timezone,
            /* [retval][out] */ DateTime *dateTime) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CallPost( 
            /* [in] */ BSTR mailer,
            /* [in] */ BSTR subject,
            /* [in] */ BSTR body,
            /* [retval][out] */ BSTR *info) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IClientVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IClient * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IClient * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IClient * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IClient * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IClient * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IClient * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IClient * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Connect )( 
            IClient * This,
            /* [in] */ BSTR serverName,
            /* [in] */ BSTR serverCallsing,
            /* [in] */ unsigned short port);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *TryConnect )( 
            IClient * This,
            /* [in] */ BSTR serverName,
            /* [in] */ BSTR serverCallsing,
            /* [in] */ unsigned short port,
            /* [retval][out] */ BOOL *connected);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *IsConnected )( 
            IClient * This,
            /* [retval][out] */ BOOL *connected);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Disconnect )( 
            IClient * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallEcho )( 
            IClient * This,
            /* [in] */ BSTR msg,
            /* [retval][out] */ BSTR *ans);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallRand )( 
            IClient * This,
            /* [in] */ int minValue,
            /* [in] */ int maxValue,
            /* [retval][out] */ int *genValue);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallTime )( 
            IClient * This,
            /* [in] */ int timezone,
            /* [retval][out] */ DateTime *dateTime);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CallPost )( 
            IClient * This,
            /* [in] */ BSTR mailer,
            /* [in] */ BSTR subject,
            /* [in] */ BSTR body,
            /* [retval][out] */ BSTR *info);
        
        END_INTERFACE
    } IClientVtbl;

    interface IClient
    {
        CONST_VTBL struct IClientVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IClient_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IClient_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IClient_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IClient_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IClient_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IClient_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IClient_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IClient_Connect(This,serverName,serverCallsing,port)	\
    ( (This)->lpVtbl -> Connect(This,serverName,serverCallsing,port) ) 

#define IClient_TryConnect(This,serverName,serverCallsing,port,connected)	\
    ( (This)->lpVtbl -> TryConnect(This,serverName,serverCallsing,port,connected) ) 

#define IClient_IsConnected(This,connected)	\
    ( (This)->lpVtbl -> IsConnected(This,connected) ) 

#define IClient_Disconnect(This)	\
    ( (This)->lpVtbl -> Disconnect(This) ) 

#define IClient_CallEcho(This,msg,ans)	\
    ( (This)->lpVtbl -> CallEcho(This,msg,ans) ) 

#define IClient_CallRand(This,minValue,maxValue,genValue)	\
    ( (This)->lpVtbl -> CallRand(This,minValue,maxValue,genValue) ) 

#define IClient_CallTime(This,timezone,dateTime)	\
    ( (This)->lpVtbl -> CallTime(This,timezone,dateTime) ) 

#define IClient_CallPost(This,mailer,subject,body,info)	\
    ( (This)->lpVtbl -> CallPost(This,mailer,subject,body,info) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IClient_INTERFACE_DEFINED__ */



#ifndef __ClientCOMLib_LIBRARY_DEFINED__
#define __ClientCOMLib_LIBRARY_DEFINED__

/* library ClientCOMLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_ClientCOMLib;

EXTERN_C const CLSID CLSID_Client;

#ifdef __cplusplus

class DECLSPEC_UUID("48505ED4-BD0B-431D-AA64-04B0A42B7317")
Client;
#endif
#endif /* __ClientCOMLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


