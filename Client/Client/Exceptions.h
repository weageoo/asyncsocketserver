#pragma once
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <exception>
#include <string>
#include <WinSock2.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define UNKNOWN_ERROR_CODE (-1)
#define UNKNOWNERR ("Unknown Error")
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class Error : public std::exception 
	{
	protected:
		std::string m_msg;
	public:
		Error()
		{
			m_msg = UNKNOWNERR;
		}
		Error(const char* msg)
		{
			m_msg = msg;
		}
		const char* GetMessage() const
		{
			return m_msg.c_str();
		}
		virtual const char* what() const throw()
		{
			return m_msg.data();
		}
	}; // End class Error

	class ThreadError : public Error
	{
	public:
		ThreadError() : Error() {}
		ThreadError(const char* msg) : Error(msg) {}
	};

	class SocketError : public Error
	{
	protected:
		int m_errorCode;
	public:
		SocketError() : Error(), m_errorCode(UNKNOWN_ERROR_CODE) {}
		SocketError(const char* msg) : Error(msg), m_errorCode(UNKNOWN_ERROR_CODE) {}
		SocketError(int code ) : Error(GetSocketErrorMessage(code)), m_errorCode(code) {} 
		SocketError(const char* msg, int code) : Error(msg), m_errorCode(code) 
		{
			m_msg += GetSocketErrorMessage(code);
		}
		int GetCode() const
		{
			return m_errorCode;
		}
		static const char* GetSocketErrorMessage(int code)
		{
			switch (code)
			{
			case WSAEINTR: 
				return "WSAEINTR"; 
			case WSAEACCES: 
				return "WSAEACCES"; 
			case WSAEFAULT:
				return "WSAEFAULT";
			case WSAEINVAL:
				return "WSAEINVAL";
			case WSAEMFILE:
				return "WSAEMFILE";
			case WSAEWOULDBLOCK:
				return "WSAEWOULDBLOCK";
			case WSAEINPROGRESS:
				return "WSAEINPROGRESS";
			case WSAEALREADY:
				return "WSAEALREADY";
			case WSAENOTSOCK:
				return "WSAENOTSOCK";
			case WSAEDESTADDRREQ:
				return "WSAEDESTADDRREQ";
			case WSAEMSGSIZE:
				return "WSAEMSGSIZE";
			case WSAEPROTOTYPE:
				return "WSAEPROTOTYPE";
			case WSAENOPROTOOPT:
				return "WSAENOPROTOOPT";
			case WSAEPROTONOSUPPORT:
				return "WSAEPROTONOSUPPORT";
			case WSAESOCKTNOSUPPORT:
				return "WSAESOCKTNOSUPPORT";
			case WSAEOPNOTSUPP:
				return "WSAEOPNOTSUPP";
			case WSAEPFNOSUPPORT:
				return "WSAEPFNOSUPPORT";
			case WSAEAFNOSUPPORT:
				return "WSAEAFNOSUPPORT";
			case WSAEADDRINUSE:
				return "WSAEADDRINUSE";
			case WSAEADDRNOTAVAIL:
				return "WSAEADDRNOTAVAIL";
			case WSAENETDOWN:
				return "WSAENETDOWN";
			case WSAENETUNREACH:
				return "WSAENETUNREACH";
			case WSAENETRESET:
				return "WSAENETRESET";
			case WSAECONNABORTED:
				return "WSAECONNABORTED";
			case WSAECONNRESET:
				return "WSAECONNRESET";
			case WSAENOBUFS:
				return "WSAENOBUFS";
			case WSAEISCONN:
				return "WSAEISCONN";
			case WSAENOTCONN:
				return "WSAENOTCONN";
			case WSAESHUTDOWN:
				return "WSAESHUTDOWN";
			case WSAETIMEDOUT:
				return "WSAETIMEDOUT";
			case WSAECONNREFUSED:
				return "WSAECONNREFUSED";
			case WSAEHOSTDOWN:
				return "WSAEHOSTDOWN";
			case WSAEHOSTUNREACH:
				return "WSAEHOSTUNREACH";
			case WSAEPROCLIM:
				return "WSAEPROCLIM";
			case WSASYSNOTREADY:
				return "WSASYSNOTREADY";
			case WSAVERNOTSUPPORTED:
				return "WSAVERNOTSUPPORTED";
			case WSANOTINITIALISED:
				return "WSANOTINITIALISED";
			case WSAEDISCON:
				return "WSAEDISCON";
			case WSATYPE_NOT_FOUND:
				return "WSATYPE_NOT_FOUND";
			case WSAHOST_NOT_FOUND:
				return "WSAHOST_NOT_FOUND";
			case WSATRY_AGAIN:
				return "WSATRY_AGAIN";
			case WSANO_RECOVERY:
				return "WSANO_RECOVERY";
			case WSANO_DATA:
				return "WSANO_DATA";
			case WSA_INVALID_HANDLE:
				return "WSA_INVALID_HANDLE";
			case WSA_INVALID_PARAMETER:
				return "WSA_INVALID_PARAMETER";
			case WSA_IO_INCOMPLETE:
				return "WSA_IO_INCOMPLETE";
			case WSA_IO_PENDING:
				return "WSA_IO_PENDING";
			case WSA_NOT_ENOUGH_MEMORY:
				return "WSA_NOT_ENOUGH_MEMORY";
			case WSA_OPERATION_ABORTED:
				return "WSA_OPERATION_ABORTED";
			case WSASYSCALLFAILURE: 
				return "WSASYSCALLFAILURE";
			}
			return UNKNOWNERR; 
		}
	}; // End class SocketError
} // End namespace sys