#ifdef _DEBUG
#import "../ClientCOM/Debug/ClientCOM.dll"
#else
#import "../ClientCOM/Release/ClientCOM.dll"
#endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <tchar.h>
#include <iostream>
#include <iomanip>
#include <locale>
#include <winerror.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
using namespace std;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const WCHAR  SERV_CALLSING[] = L"AFEE9508-8716-4302-AF37-2E334F6922F3";
static const WCHAR  SERV_NAME[]     = L"localhost";
static const USHORT SERV_PORT       = 10000;

int _tmain(int argc, _TCHAR* argv[])
{
	::setlocale (LC_ALL, "Russian");
	
	CoInitialize(NULL);
	ClientCOMLib::IClientPtr pClient(__uuidof(ClientCOMLib::Client));			
	int randVal;
	bstr_t ans;
	ClientCOMLib::DateTime dtVal;

	//while (true)
	//{
		try
		{
			if ( pClient->TryConnect(SERV_NAME, SERV_CALLSING, SERV_PORT) )
			{

				for (int i = 0; pClient->IsConnected() && i < 1000; ++i)
				{
					system("cls");

					randVal = pClient->CallRand(-100, 100);

					cout << "Rand[-100, 100]: " << randVal << endl;

					dtVal = pClient->CallTime(+120);

					cout << "Time[timezone +120]: "
						<< setw(2) << setfill('0') << dtVal.Hour 
						<< ":" << setw(2) << setfill('0') << dtVal.Minute 
						<< ":" << setw(2) << setfill('0') << dtVal.Second << endl;

					cout << "ReDraw: " << i;
				}
				cout << "\nClient end." << endl;
			}
		}
		catch (const _com_error& ex)
		{
			cout << ex.ErrorMessage() << endl;
			cout << "Секундное ожидание...\r\n";
			::Sleep(1000);
		}
		catch (...)
		{
			cout << "Unknown Error" << endl; 
		}
	//}

	system("pause");
	return 0;
}