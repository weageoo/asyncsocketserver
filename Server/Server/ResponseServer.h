#pragma once
#include "Server.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <WinSock2.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#pragma comment(lib, "Ws2_32.lib")
#pragma warning(disable:4127)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class ResponseServer : public Server
	{
	private:
		HANDLE        m_hThread;    // ��������� ������
		volatile BOOL m_isRun;      // ������� �� ������?
	public:
		ResponseServer();
	   ~ResponseServer();
		VOID Start(INT threadPriority);
		VOID Start();
		VOID Stop(DWORD waitTimeMsec);
		VOID Stop();
		BOOL IsStarted() const;
		BOOL SetPriority(INT);
		INT  GetPriority() const;
		VOID WaitServ() const;    // �������, ���� �� ���������� ������
		VOID ExecuteCommand(REMOTE_COMMANDS, COMMAND_PARAMS, ANSWER&);
	private:
		static UINT WINAPI NetThread(PVOID lpParam); // ������� ������
	};
}