#pragma once
#include "Converter.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <windows.h>
#include <string>

namespace sys
{
	// �������������� ���������� � �������.
	// ���������������� �����.
	class ServerStatistics
	{
	public:
		enum CLIENTS_TYPE
		{
			CURRENT_CLIENTS, SUCCESS_CLIENTS, VALID_CLIENTS
		};
	private:
		volatile LONG m_currentClients;
		volatile LONG m_successClients;
		volatile LONG m_validClients;
		volatile LONG m_timeoutClients;
	public:
		ServerStatistics();
		~ServerStatistics(){}
		VOID IncrementClient(CLIENTS_TYPE cType);
		VOID IncrementClientNoRecalc(CLIENTS_TYPE cType); // ���������������� ��� ���������
		LONG GetNumberOfClients(CLIENTS_TYPE cType);
		VOID Clear();
		std::string ToString() const;
	};
}