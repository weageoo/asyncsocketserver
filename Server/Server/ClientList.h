#pragma once
#include "ClientContext.h"
#include "CriticalSection.h"
#include "ServerStatistics.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <vector>
#include <algorithm>
#include <process.h>

namespace sys
{
	class ClientList
	{
		typedef std::vector<ClientContext*> CLIENT_LIST;
	private:
		CLIENT_LIST      m_clients;     // ������ ��������
		CriticalSection  m_cs;          // ����������� ������ ��� ������ ��������
		ServerStatistics m_statistics;  // ����������
		HANDLE           m_hGCThread;   // ��������� ������ �������
		HANDLE           m_hGCTimer;    // ������ ��� ������������� ������� ������
		volatile ULONG   m_sessionTime; // ������������ ����� ����������� �������
		volatile BOOL    m_isGCRun;     // ������� �� GC?
	public:
		ClientList();
		~ClientList();
		VOID Add(ClientContext *pContext);
		VOID Remove(ClientContext *pContext, ServerStatistics::CLIENTS_TYPE clientType);
		VOID Clear();
		UINT Count() const;
		VOID  SetSessionTime(ULONG sessionTimeMsec);
		ULONG GetSessionTime() const;
		VOID  StartGC();
		VOID  StopGC();
		ServerStatistics GetStatistics() const;
	private:
		static UINT WINAPI GCThread(PVOID pPrm); // ������� ������ ��� ������� ��������
		VOID x_Clear(); // �������� �������
	};
}