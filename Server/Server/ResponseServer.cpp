#include "ResponseServer.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <process.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define CHECK_INTERVAL_MSEC (300)   // ����� ������� select
#define MAX_CALLSING_LEN    (100)   // ������������ ����� ���������
#define MAX_ENDSERV_TIME    (1000)  // ������������ ����� �������� ���������� ������� (� �����������) 
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	ResponseServer::ResponseServer()
	{
		m_hThread  = INVALID_HANDLE_VALUE;
		m_isRun    = FALSE;
	}

	ResponseServer::~ResponseServer()
	{ 
		if (IsStarted())
			this->Stop(MAX_ENDSERV_TIME);
	}

	VOID ResponseServer::Start(INT nPriority)
	{
		if (IsStarted())
			throw Error("ResponseServer::Start(): The server is already started");	

		if (m_params.IsCorrectlyInit() == FALSE)
			throw Error("ResponseServer::Start(): The server parameters incorrect");

		try
		{
			m_hThread = (HANDLE)::_beginthreadex(
				NULL, NULL, NetThread, static_cast<PVOID>(this), CREATE_SUSPENDED, NULL);
			if (m_hThread == INVALID_HANDLE_VALUE)
				throw ThreadError("The working stream of a server has not been created");

			if (::SetThreadPriority(m_hThread, nPriority) == FALSE)
				throw ThreadError("SetThreadPriority() error");

			m_isRun = TRUE;

			::ResumeThread(m_hThread);
		}
		catch (...)
		{
			if (m_hThread != INVALID_HANDLE_VALUE)
				this->Stop();
			throw;
		}
	}

	VOID ResponseServer::Start()
	{
		this->Start(THREAD_PRIORITY_NORMAL);
	}

	VOID ResponseServer::Stop(DWORD waitTimeMsec)
	{
		if (IsStarted())
		{
			::InterlockedExchange((LONG*)&m_isRun, FALSE);
			
			switch (::WaitForSingleObject(m_hThread, waitTimeMsec))
			{
				case WAIT_OBJECT_0:
					::CloseHandle(m_hThread);
					break;
				case WAIT_TIMEOUT:
					::TerminateThread(m_hThread, WAIT_TIMEOUT);
					::CloseHandle(m_hThread);
					break;
			}

			m_hThread = INVALID_HANDLE_VALUE;
		}
	}

	VOID ResponseServer::Stop()
	{
		this->Stop(INFINITE);
	}

	BOOL ResponseServer::IsStarted() const
	{
		return m_isRun;
	}

	BOOL ResponseServer::SetPriority(INT nPriority)
	{
		if (m_hThread == INVALID_HANDLE_VALUE || !::SetThreadPriority(m_hThread, nPriority)) 
			return FALSE;
		return TRUE;
	}
	INT  ResponseServer::GetPriority() const
	{
		return (IsStarted()) ? (::GetThreadPriority(m_hThread)) : (0);
	}

	VOID ResponseServer::WaitServ() const
	{
		if (m_hThread != INVALID_HANDLE_VALUE)
			::WaitForSingleObject(m_hThread, INFINITE);
	}

	VOID ResponseServer::ExecuteCommand(REMOTE_COMMANDS cmd, COMMAND_PARAMS prms, ANSWER& answer)
	{
		COMMAND_PARAMS::iterator it;

		switch (cmd)
		{
		case START_REMOTE_COMMAND:
			if (IsStarted())
				answer.info = "ResponseServer is already started.";
			else
			{
				if ((it = prms.find("response_priority")) != prms.end())
					this->Start(Convert::ToInt32(it->second));
				else 
					this->Start();
				answer.info = "ResponseServer started successfully.";
			}
			break;
		case STOP_REMOTE_COMMAND:
		case SHUTDOWN_REMOTE_COMMAND:
		case EXIT_REMOTE_COMMAND:
			if ((it = prms.find("timeout")) != prms.end())
				this->Stop(Convert::ToInt32(it->second));
			else
				this->Stop(); 
			answer.info = "ResponseServer stopped successfully.";
			break;
		}
	}

	UINT WINAPI ResponseServer::NetThread(PVOID lpParam)
	{
		SOCKET sS = NULL;                 // ����� ��� ����� � ������� �������� UDP
		WSADATA wsaData = {NULL};         // ��������� ���������� � Winsock
		SOCKADDR_IN localAddr;            // ��������� ������ �������
		SOCKADDR clientAddr;              // ��������� ������ �������
		INT addrLen = sizeof(SOCKADDR);   // ����� sockaddr
		CHAR ibuf[MAX_CALLSING_LEN] = ""; // ����� ��� ����� ��������
		INT ibuflen = 0;                  // ����� ��������� ���������
		FD_SET readSet;                   // ����� ������� FD_SET, ��������� ��� ������
		LPCSTR callsing;                  // �������� �������
		ResponseServer* rServ = NULL;     // ��������� �� ��������� ������, ��������������� �����
		ServerParams sParams;             // ��������� ������� (ip, port, callsing)
		EVENT_ARG eArg;                   // �������� �������
		static TIMEVAL tmv = {            // �������� �������� select
			0, CHECK_INTERVAL_MSEC * 1000};

		SeTranslator::SetDefaultTransFuncForCurrentThread();

		try
		{
			rServ = static_cast<ResponseServer*>(lpParam); // ��������� �� "��������� �����"
			rServ->PulseEvent(SERVER_START_EVENT, rServ, eArg);

			::ZeroMemory(&clientAddr, addrLen);

			if(::WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
				throw SocketError("Error occurred while WSAStartup(): ", ::WSAGetLastError());

			// << ������������� ���������� ��� ������ ������ �� "�����������" �����

			sParams   = rServ->GetParams();
			localAddr = sParams.GetSockaddr_in();
			callsing  = sParams.GetCallsing();

			if ((sS = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) == INVALID_SOCKET)
				throw SocketError("Error occurred while socket(): ", ::WSAGetLastError());

			ULONG ulBlock = 1; // ������������� ����� (�������� ������� FIONBIO)
			if (::ioctlsocket(sS, FIONBIO, &ulBlock) == SOCKET_ERROR)
				throw SocketError("Error occurred while ioctlsocket(): ", ::WSAGetLastError());

			if (::bind(sS, (PSOCKADDR)&localAddr, addrLen) == SOCKET_ERROR)
				throw SocketError("Error occurred while bind(): ", ::WSAGetLastError());

			while (rServ->IsStarted()) // ���� ����� "������������" ������ ���������� TRUE
			{
				FD_ZERO(&readSet);
				FD_SET(sS, &readSet);

				if (::select(0, &readSet, NULL, NULL, &tmv) == SOCKET_ERROR)
					throw SocketError("Error occurred while select(): ", ::WSAGetLastError());

				if (FD_ISSET(sS, &readSet))
				{
					ibuflen = ::recvfrom(sS, ibuf, sizeof(ibuf), NULL, &clientAddr, &addrLen);
					if ( (ibuflen == SOCKET_ERROR) && (::WSAGetLastError() != WSAECONNRESET) )
							throw SocketError("Error occurred while recvfrom(): ", ::WSAGetLastError());
					if (ibuflen > 0 && ::strcmp(callsing, ibuf) == 0)
						if( (::sendto(sS, callsing, ::strlen(callsing), NULL, &clientAddr, addrLen)
							) == SOCKET_ERROR && (::WSAGetLastError() != WSAECONNRESET) )
								throw SocketError("Error occurred while sendto(): ", ::WSAGetLastError());

					::ZeroMemory(&clientAddr, addrLen);
				}
			}
		}
		catch (const SocketError& ex)
		{
			eArg.message = ex.what();
			eArg.error_code = ex.GetCode();
		}
		catch (const std::exception& ex)
		{
			eArg.message = ex.what();
			eArg.error_code = UNKNOWN_ERROR_CODE;
		}
		catch (...)
		{
			eArg.message = UNKNOWN_ERROR_STR;
			eArg.error_code = UNKNOWN_ERROR_CODE;
		}

		if (sS != NULL)
		{
			::shutdown(sS, SD_BOTH);
			::closesocket(sS);
		}
		if (wsaData.wVersion != NULL)
		{
			::WSACleanup();
		}

		::InterlockedExchange((PLONG)&rServ->m_isRun, FALSE);

		rServ->PulseEvent(SERVER_STOP_EVENT, rServ, eArg);

		::_endthreadex(0);
		return 0;
	}
}

#undef CHECK_INTERVAL_MSEC
#undef MAX_CALLSING_LEN
#undef MAX_ENDSERV_TIME