#pragma once
#include <string>

namespace sys
{
	class Convert
	{
	public:
		static __int32 ToInt32(std::string str)
		{
			return ::atoi(str.c_str());
		}
		static __int64 ToInt64(std::string str)
		{
			return ::_atoi64(str.c_str());
		}
		static std::string ToStr(long val)
		{
			char buffer[50];
			_ltoa_s(val, buffer, sizeof(buffer), 10);
			return buffer;
		}
		static std::string ToStr(unsigned long val)
		{
			char buffer[50];
			_ultoa_s(val, buffer, sizeof(buffer), 10);
			return buffer;
		}
	};
}