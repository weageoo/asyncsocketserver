#include "CriticalSection.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	CriticalSection::CriticalSection()
	{
		::InitializeCriticalSection(&m_cs);
	}

	CriticalSection::~CriticalSection()
	{
		::DeleteCriticalSection(&m_cs);
	}

	VOID CriticalSection::Enter()
	{
		::EnterCriticalSection(&m_cs);
	}

	BOOL CriticalSection::TryEnter()
	{
		return ( ::TryEnterCriticalSection(&m_cs) );
	}

	VOID CriticalSection::Leave()
	{
		::LeaveCriticalSection(&m_cs);
	}
}