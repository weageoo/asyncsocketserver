#include "ClientContext.h"

namespace sys
{
	ClientContext::ClientContext()
	{
		m_pol   = new OVERLAPPED;
		m_pwbuf = new WSABUF;

		::ZeroMemory(m_pol, sizeof(OVERLAPPED));
		::ZeroMemory(m_szBuffer, MAX_BUFFER_LEN);

		m_Socket = INVALID_SOCKET;

		m_pwbuf->buf = m_szBuffer;
		m_pwbuf->len = MAX_BUFFER_LEN;

		m_nOpCode     = OP_NONE;
		m_nTotalBytes = 0;
		m_nSentBytes  = 0;

		m_lastQuery = ::GetTickCount();
	}

	ClientContext::~ClientContext()
	{
		/* // ������� ���������� ������ � ��������
		   while (!HasOverlappedIoCompleted(m_pol)) 
			   ::Sleep(0); 
		*/

		Disconnect();

		delete m_pol;
		delete m_pwbuf;
	}

	// Get-Set-Is

	VOID ClientContext::SetOpCode(OPCODES code)
	{
		m_nOpCode = code;
	}

	INT ClientContext::GetOpCode()
	{
		return m_nOpCode;
	}

	VOID ClientContext::SetTotalBytes(INT n)
	{
		m_nTotalBytes = n;
	}

	INT ClientContext::GetTotalBytes()
	{
		return m_nTotalBytes;
	}

	VOID ClientContext::SetSentBytes(INT n)
	{
		m_nSentBytes = n;
	}

	VOID ClientContext::IncrSentBytes(INT n)
	{
		m_nSentBytes += n;
	}

	INT ClientContext::GetSentBytes()
	{
		return m_nSentBytes;
	}

	VOID ClientContext::SetSocket(SOCKET s)
	{
		m_Socket = s;
	}

	SOCKET ClientContext::GetSocket()
	{
		return m_Socket;
	}

	VOID ClientContext::SetBuffer(PSTR szBuffer)
	{
		::strcpy_s(m_szBuffer, MAX_BUFFER_LEN, szBuffer);
	}

	VOID ClientContext::GetBuffer(PSTR szBuffer)
	{
		::strcpy_s(szBuffer, MAX_BUFFER_LEN, m_szBuffer);
	}

	VOID ClientContext::ZeroBuffer()
	{
		::ZeroMemory(m_szBuffer, MAX_BUFFER_LEN);
	}

	VOID ClientContext::SetWSABUFLength(INT nLength)
	{
		m_pwbuf->len = nLength;
	}

	INT ClientContext::GetWSABUFLength()
	{
		return m_pwbuf->len;
	}

	DWORD ClientContext::GetLastQueryTime()
	{
		return m_lastQuery;
	}

	VOID ClientContext::Update()
	{
		m_lastQuery = ::GetTickCount();
	}

	VOID ClientContext::Disconnect()
	{
		if (m_Socket != INVALID_SOCKET)
		{
			::shutdown(m_Socket, SD_BOTH);
			::closesocket(m_Socket);
		}
	}

	LPWSABUF ClientContext::GetWSABUFPtr()
	{
		return m_pwbuf;
	}

	LPOVERLAPPED ClientContext::GetOVERLAPPEDPtr()
	{
		return m_pol;
	}

	VOID ClientContext::ResetWSABUF()
	{
		ZeroBuffer();
		m_pwbuf->buf = m_szBuffer;
		m_pwbuf->len = MAX_BUFFER_LEN;
	}
}