#include "ServerParams.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define ANY_IP_STR "0.0.0.0"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	ServerParams::ServerParams(LPCSTR ip, USHORT port, LPCSTR callsing, ULONG sessionTimeMsec)
	{
		SetIP(ip);
		SetPort(port);
		SetCallsing(callsing);
		SetSessionTime(sessionTimeMsec);
	}

	ServerParams::ServerParams(USHORT port, LPCSTR callsing, ULONG sessionTimeMsec)
	{
		ServerParams(ANY_IP_STR, port, callsing, sessionTimeMsec);
	}

	ServerParams::ServerParams()
	{
		m_callsing = NULL;
		m_ip       = NULL;
		m_port     = 0;
		m_session  = 0;
	}

	BOOL ServerParams::IsCorrectlyInit()
	{
		if (m_callsing == NULL || m_ip == NULL || m_port == 0) 
			return FALSE;
		return TRUE;
	}

	SOCKADDR_IN ServerParams::GetSockaddr_in() const
	{
		if (m_ip == NULL || m_port == 0)
			throw Error("BroadcastServer::GetSockaddr_in(): IP or port not set or incorrect");

		SOCKADDR_IN addr;
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = ::inet_addr(m_ip);
		addr.sin_port = ::htons(m_port);
		return addr;
	}

	VOID ServerParams::SetCallsing(LPCSTR callsing)
	{
		if (::strlen(callsing) <= 0)
			throw Error("BroadcastServer::SetCallsing(): The callsign does not contain symbols");

		m_callsing = callsing;
	}

	VOID ServerParams::SetIP(LPCSTR ip)
	{
		if (::strlen(ip) <= 0)
			throw Error("BroadcastServer::SetIP(): The ip does not contain symbols");

		m_ip = ip;
	}

	VOID ServerParams::SetPort(USHORT port)
	{
		if (port == 0)
			throw Error("BroadcastServer::SetPort(): It is impossible to use port with number 0");

		m_port = port;
	}

	VOID ServerParams::SetSessionTime(ULONG sessionTimeMsec)
	{
		m_session = sessionTimeMsec;
	}

	LPCSTR ServerParams::GetCallsing() const
	{
		return m_callsing;
	}

	LPCSTR ServerParams::GetIP() const
	{
		return m_ip;
	}

	USHORT ServerParams::GetPort() const
	{
		return m_port;
	}

	ULONG ServerParams::GetSessionTime() const
	{
		return m_session;
	}
}