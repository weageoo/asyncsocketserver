#pragma once
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <tchar.h>
#include <exception>
#include <string>
#include <WinSock2.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const int   UNKNOWN_ERROR_CODE = (-1);
static const TCHAR UNKNOWN_ERROR_STR[] = _TEXT("Unknown Error");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class SeTranslator
	{
	public:
		// ���������� ������� ���������� SE-���������� ��� �������� ������
		static void SetDefaultTransFuncForCurrentThread();
		static void SetSpecificTransFuncForCurrentThread(_se_translator_function);
	private:
		// ������� ���������� SEH-���������� � ���������� C++
		static void xs_SeTranslator(UINT, PEXCEPTION_POINTERS);
	};

	class Error : public std::exception
	{
	protected:
		std::string m_msg;
	public:
		Error();
		Error(const char* msg);
		const char* GetMessage() const;
		virtual const char* what() const throw();
	};

	class ThreadError : public Error
	{
	public:
		ThreadError();
		ThreadError(const char* msg);
	};

	class SocketError : public Error
	{
	protected:
		int m_errorCode;
	public:
		SocketError();
		SocketError(const char* msg);
		SocketError(int code);
		SocketError(const char* msg, int code);
		int GetCode() const;
		static const char* GetSocketErrorMessage(int code);
	};
}