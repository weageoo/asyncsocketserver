#pragma once
#include "Exceptions.h"
#include "CriticalSection.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <map>
#include <string>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	struct EVENT_ARG // �������� �������
	{
		std::string message;
		long        error_code;

		EVENT_ARG() : message(TEXT("")), error_code(0) { }
	};

	typedef VOID (*EVENT_HANDLER)(PVOID sender, EVENT_ARG e); // ��� ����������� �������

	enum EVENT_TYPE
	{
		SERVER_START_EVENT, // ������ ������� 
		SERVER_STOP_EVENT,  // ������ ����������
		SERVER_INFO_EVENT   // ������������� ��������� �� �������
	};

	class EventArray
	{
	private:
		std::multimap<EVENT_TYPE, EVENT_HANDLER> m_events;
	public:
		EventArray();
		VOID Add(EVENT_HANDLER, EVENT_TYPE);    // ������������ ����������
		VOID Remove(EVENT_HANDLER, EVENT_TYPE); // ����������� ����������
		VOID Pulse(EVENT_TYPE eType, PVOID sender, EVENT_ARG eArg) const; // ���������� �����������
	};

	// ��������� �������.
	class IEvents
	{
	public:
		// ���������� ���������� �������.
		virtual VOID AddEventHandler(EVENT_HANDLER, EVENT_TYPE) = 0;
		// ��������� ���������� �������.
		virtual VOID RemoveEventHandler(EVENT_HANDLER, EVENT_TYPE) = 0;
	};

	class EventHelper : IEvents
	{
	private:
		EventArray  m_events;        // ������ ���������� �� ����������� �������
		static CriticalSection m_cs; // ������ ������������� - ���������� ������������� �������
	public:
		// ���������� ���������� �������.
		virtual VOID AddEventHandler(EVENT_HANDLER, EVENT_TYPE);
		// ��������� ���������� �������.
		virtual VOID RemoveEventHandler(EVENT_HANDLER, EVENT_TYPE);
	protected:
		// ���������������� ���������� �������.
		virtual VOID PulseEvent(EVENT_TYPE eType, PVOID sender, EVENT_ARG eArg);
	private:
		static UINT WINAPI EventThread(PVOID pPrm);
	};
}