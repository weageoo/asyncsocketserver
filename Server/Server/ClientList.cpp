#include "ClientList.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define TIMEOUT_MSEC 3000   // ������ ������������ �������

namespace sys
{
	ClientList::ClientList()
	{
		m_sessionTime = INFINITE;

		m_hGCTimer = ::CreateWaitableTimer(NULL, FALSE, NULL);

		if (m_hGCTimer == NULL)
			throw Error("CreateWaitableTimer failed.");

		m_hGCThread = INVALID_HANDLE_VALUE;
		m_isGCRun = FALSE;
	}
	ClientList::~ClientList()
	{
		StopGC();
		::CloseHandle(m_hGCTimer);
	}
	VOID ClientList::StartGC()
	{
		if (!m_isGCRun)
		{
			m_hGCThread = (HANDLE)::_beginthreadex(NULL, 0, GCThread, this, CREATE_SUSPENDED, NULL);
			::InterlockedExchange((PLONG)&m_isGCRun, TRUE);
			::SetThreadPriority(m_hGCThread, THREAD_PRIORITY_LOWEST);
			::ResumeThread(m_hGCThread);
		}
	}
	VOID ClientList::StopGC()
	{
		if (m_isGCRun)
		{
			LARGE_INTEGER liDueTime;
			liDueTime.QuadPart = 0;
			::InterlockedExchange((PLONG)&m_isGCRun, FALSE);
			::SetWaitableTimer(m_hGCTimer, &liDueTime, 0, NULL, NULL, FALSE);
			::WaitForSingleObject(m_hGCThread, INFINITE);
			::CloseHandle(m_hGCThread);
		}
	}
	VOID ClientList::Add(ClientContext *pContext)
	{
		m_cs.Enter();
		m_clients.push_back(pContext);
		m_cs.Leave();

		m_statistics.IncrementClient(ServerStatistics::CURRENT_CLIENTS);
	}
	VOID ClientList::Remove(ClientContext *pContext, ServerStatistics::CLIENTS_TYPE clientType)
	{
		m_cs.Enter();
		{
			CLIENT_LIST::iterator it = 
				std::find(m_clients.begin(), m_clients.end(), pContext);

			if (it != m_clients.end())
			{
				delete pContext;
				m_clients.erase(it);
				m_statistics.IncrementClient(clientType);
			}
		}
		m_cs.Leave();
	}
	VOID ClientList::Clear()
	{
		m_cs.Enter();
		{
			for (CLIENT_LIST::iterator it = m_clients.begin(); it != m_clients.end(); ++it)
				delete *it; // �������� ����������� ClientContext

			m_clients.clear();

			m_cs.Leave();
		}
		m_statistics.Clear();
	}

	UINT ClientList::Count() const
	{
		return m_clients.size();
	}

	VOID ClientList::SetSessionTime(ULONG sessionTimeMsec)
	{
		m_cs.Enter();
		{
			m_sessionTime = sessionTimeMsec;

			LARGE_INTEGER liDueTime;
			liDueTime.QuadPart = (-10000) * TIMEOUT_MSEC;
			::SetWaitableTimer(m_hGCTimer, &liDueTime, TIMEOUT_MSEC, NULL, NULL, FALSE);
		}
		m_cs.Leave();
	}

	ULONG ClientList::GetSessionTime() const
	{
		return m_sessionTime;
	}

	ServerStatistics ClientList::GetStatistics() const
	{
		return m_statistics;
	}

	UINT WINAPI ClientList::GCThread(PVOID pPrm)
	{
		ClientList* cList = static_cast<ClientList*>(pPrm);

		__try
		{
			while(cList->m_isGCRun && ::WaitForSingleObject(cList->m_hGCTimer, INFINITE) == WAIT_OBJECT_0)
			{
				if (cList->m_isGCRun)
					cList->x_Clear();
			}
		}
		__finally
		{
			if (cList->m_isGCRun)
				::InterlockedExchange((PLONG)&cList->m_isGCRun, FALSE);
		}
		::_endthreadex(0);
		return 0;
	}

	VOID ClientList::x_Clear()
	{
		if (m_cs.TryEnter())
		{
			DWORD tickCount = ::GetTickCount();

			for (CLIENT_LIST::iterator it = m_clients.begin(); it != m_clients.end(); )
			{
				if ( (tickCount - (**it).GetLastQueryTime()) > m_sessionTime )
				{
					(**it).Disconnect(); // ��������� ���������� � ��������, ����������� ��������
				}
				if (it != m_clients.end())
					++it;
			}		
			m_cs.Leave();
		}
	}
}

#undef TIMEOUT_MSEC