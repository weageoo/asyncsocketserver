#include "Server.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
namespace sys
{
	VOID Server::SetParams(ServerParams sParams)
	{
		if (!sParams.IsCorrectlyInit())
			throw Error("ResponseServer::SetParams(): server parameters incorrect");

		m_params = sParams; // ������� ������� ! (���������, ������� �� ������ ?)
	}

	ServerParams Server::GetParams() const
	{
		return m_params;
	}
}