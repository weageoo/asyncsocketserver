#include "ConsolePipe.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define MAX_ENDSERV_TIME (500) // ������������ ����� �������� ���������� ������� (� �����������)
#define CS_TIMEOUT       (300) // ������ ������ ����� ���������� ��� �������� �����������
#define RS_TIMEOUT       (300) // ������ ������ ����� ���������� ��� �������� ������ � �����
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	ConsolePipe::ConsolePipe()
	{
		m_hThread  = INVALID_HANDLE_VALUE;
		m_hNp      = INVALID_HANDLE_VALUE;
		m_pipeName = "";
		m_isRun    = FALSE;	
	}

	ConsolePipe::~ConsolePipe()
	{
		if (IsStarted())
			this->Stop(MAX_ENDSERV_TIME);
	}

	VOID ConsolePipe::Start(INT nPriority)
	{
		if (IsStarted())
			throw Error("ConsolePipe::Start(): The server is already started.");	

		if (::strcmp(m_pipeName, "") == 0)
			throw Error("Pipe name no set.");

		try
		{
			m_hThread = (HANDLE)::_beginthreadex(
				NULL, NULL, NetThread, static_cast<PVOID>(this), CREATE_SUSPENDED, NULL);
			if (m_hThread == INVALID_HANDLE_VALUE)
				throw ThreadError("The working stream of a server has not been created");

			if (::SetThreadPriority(m_hThread, nPriority) == FALSE)
				throw ThreadError("SetThreadPriority() error");

			m_isRun = true;

			::ResumeThread(m_hThread);
		}
		catch (...)
		{
			if (m_hThread != NULL)
				this->Stop();
			throw;
		}
	}

	VOID ConsolePipe::Start()
	{
		Start(THREAD_PRIORITY_NORMAL);
	}

	BOOL ConsolePipe::IsStarted() const
	{
		return m_isRun;
	}

	VOID ConsolePipe::Stop(DWORD waitTimeMsec)
	{
		if (IsStarted())
		{
			::InterlockedExchange((LONG*)&m_isRun, FALSE);

			if (::GetThreadId(m_hThread) != ::GetCurrentThreadId())
				switch (::WaitForSingleObject(m_hThread, waitTimeMsec))
				{
				case WAIT_OBJECT_0:
					::CloseHandle(m_hThread);
					break;
				case WAIT_TIMEOUT:
					::TerminateThread(m_hThread, 0);
					::CloseHandle(m_hThread);
					break;
				}

			m_hThread = INVALID_HANDLE_VALUE;
		}
	}

	VOID ConsolePipe::Stop()
	{
		this->Stop(INFINITE);
	}

	BOOL ConsolePipe::SetPriority(INT nPriority)
	{
		if (m_hThread == INVALID_HANDLE_VALUE || !::SetThreadPriority(m_hThread, nPriority)) 
			return FALSE;
		return TRUE;
	}

	INT  ConsolePipe::GetPriority() const
	{
		return (IsStarted()) ? (::GetThreadPriority(m_hThread)) : (0);
	}

	VOID ConsolePipe::SetPipeName(LPCSTR pipeName)
	{
		if (pipeName == NULL || (::strcmp(pipeName, "") == 0))
			throw Error("Pipe name not support");
		m_pipeName = pipeName;
	}

	LPCSTR ConsolePipe::GetPipeName() const
	{
		return m_pipeName;
	}

	VOID ConsolePipe::LinkService(IRemoteCommands* serv)
	{
		m_servs.push_back(serv);
	}

	VOID ConsolePipe::WaitServ() const
	{
		if (m_hThread != INVALID_HANDLE_VALUE)
			::WaitForSingleObject(m_hThread, INFINITE);
	}

	VOID ConsolePipe::ExecuteRemoteCommand(LPCSTR cmdStr, LPSTR report)
	{
		REMOTE_COMMANDS cmd = NULL_REMOTE_COMMAND;
		COMMAND_PARAMS prms;
		
		try
		{
			// << �������� ������ ���������� ������

			int i = 0;
			std::string cmdName, cmdPrm, cmdVal;

			while (cmdStr[i] != ' ' && cmdStr[i] != '\0')
				cmdName += (cmdStr[i++]);

			for (int i = CMDSTRLIST_SIZE; --i >= 0 && cmd == NULL_REMOTE_COMMAND; )
				if (cmdName == std::string(CMDSTRLIST[i].cmdname))
					cmd = CMDSTRLIST[i].command;
			if (cmd == NULL_REMOTE_COMMAND)
				throw Error("Command not support.");

			while (cmdStr[++i] == ' ');     // �����
			while (cmdStr[i] == '-')
			{
				while (cmdStr[++i] != ' ')
					cmdPrm += (cmdStr[i]);
				while (cmdStr[++i] == ' '); // �����
				while (cmdStr[i] != ' ' && cmdStr[i] != '\0')
					cmdVal += (cmdStr[i++]);
				while (cmdStr[++i] == ' '); // �����

				prms.insert(std::make_pair(cmdPrm, cmdVal));
				cmdPrm.clear(); cmdVal.clear();
			}

			// << ������ �������. ��������� � ����������

			if (cmd == NULL_REMOTE_COMMAND)
				return; // �������, ���� �� ���� �� ������ �� ����������

			ANSWER answ;
			std::string totalAnswer;
			std::list<ANSWER> answers;
			std::list<ANSWER>::iterator pos;
			for (u_int i = 0; i < m_servs.size(); ++i)
			{
				m_servs[i]->ExecuteCommand(cmd, prms, answ);
				answers.push_back(answ);
			}
			for (pos = answers.begin(); pos != answers.end(); ++pos)
				if (pos->info.length() > 0)
					totalAnswer += "Info: \r\n" + pos->info + "\r\n";
			for (pos = answers.begin(); pos != answers.end(); ++pos)
				if (pos->error.length() > 0)
					totalAnswer += "Error: \r\n" + pos->error + "\r\n";

			::memcpy(report, totalAnswer.c_str(), totalAnswer.size());
			
			if (cmd == EXIT_REMOTE_COMMAND) 
				Stop();
		}
		catch (const std::exception& ex)
		{
			::memcpy(report, ex.what(), ::strlen(ex.what()));
		}
	}

	UINT WINAPI ConsolePipe::NetThread(PVOID pParam)
	{
		HANDLE hConTh = INVALID_HANDLE_VALUE;   // ��������� ������ �����������
		HANDLE hRdTh = INVALID_HANDLE_VALUE;    // ��������� ������ ������ (��������� ReadFile)
		ReadNpThArg readThArgs = {0};           // ��������� ��� ������ ������
		DWORD dwBytesWrite = 0;                 // ��������, ����
		CHAR buffer[256] = "";                  // ����� ��� ������ (������ ������)
		CHAR resBuf[1024] = "";                 // ����� � ���������� �������
		ConsolePipe* cServ = NULL;              // ��������� �� ��������� ������, ��������������� �����
		SECURITY_ATTRIBUTES securityAttr;       // �������� ������������ ������
		SECURITY_DESCRIPTOR securityDesc;       // ���������� ������������ ������
		EVENT_ARG eArg;                         // �������� �������� �������
		BOOL success = TRUE;                    // ��������� �� ������������ ���������� ReadFile, WriteFile

		SeTranslator::SetDefaultTransFuncForCurrentThread();

		try
		{
			cServ = static_cast<ConsolePipe*>(pParam);

			cServ->PulseEvent(SERVER_START_EVENT, cServ, eArg);

			/// <!-- ��������� ��������� ������ ������������ ������

			// �������������� ���������� ������������
			::InitializeSecurityDescriptor(&securityDesc, SECURITY_DESCRIPTOR_REVISION);
			// ��������� ������ � ������������ ������ ���� �������������
			::SetSecurityDescriptorDacl(&securityDesc, TRUE, NULL, FALSE);

			securityAttr.nLength = sizeof(securityAttr);
			securityAttr.bInheritHandle = FALSE; // ���������� ������ �������������
			securityAttr.lpSecurityDescriptor = &securityDesc;

			/// -->

			while (cServ->IsStarted())
			{
				cServ->m_hNp = ::CreateNamedPipe(
					// �������� ������������ ������ � ������� UNC
					cServ->GetPipeName(),
					// ����������� �������� (� ����� ����� �����-������ � ������������) - ��������������� (duplex)                         
					PIPE_ACCESS_DUPLEX,                  
					// ������ �������� ������, ������ � �������� - ������ � ������ �����������, ����� ������������ ��������
					PIPE_READMODE_MESSAGE | PIPE_TYPE_MESSAGE | PIPE_WAIT,
					1,            // ������������ ���-�� ����������� ������ (����. ���-�� �������. ��������)
					0, 0,         // ������� ��������� � �������� ������� ��������������
					INFINITE,     // ������ ��� ����� ���������� �����
					&securityAttr // ���������� ������������ ������������ ������
					);
				if (cServ->m_hNp == INVALID_HANDLE_VALUE)
					throw Error("CreateNamedPipe failed.");

				while (success && cServ->IsStarted())
				{
					// << ������������� ����������

					hConTh = (HANDLE)::_beginthreadex(NULL, 0, ConnectTh, (PVOID)cServ, 0, NULL);
					while (cServ->IsStarted() && // �������� ����������� + ����� ����� ����������
						::WaitForSingleObject(hConTh, CS_TIMEOUT) == WAIT_TIMEOUT);
					if (!cServ->IsStarted()) 
						::TerminateThread(hConTh, 0);
					else ::CloseHandle(hConTh);

					// << ������-������

					while (success && cServ->IsStarted())
					{
						readThArgs.hNp = &cServ->m_hNp;
						readThArgs.buffer = buffer;
						readThArgs.bufSize = sizeof(buffer);

						hRdTh = (HANDLE)::_beginthreadex(NULL, 0, ReadNpTh, (PVOID)&readThArgs, 0, NULL);
						while (cServ->IsStarted() && // �������� ������ � ����� + ����� ����� ����������
							::WaitForSingleObject(hRdTh, RS_TIMEOUT) == WAIT_TIMEOUT);
						if (!cServ->IsStarted())
							::TerminateThread(hRdTh, 0);
						else ::CloseHandle(hRdTh);
							

						if (success && cServ->IsStarted())
						{
							cServ->ExecuteRemoteCommand(buffer, resBuf);

							try {
								success = ::WriteFile(
									cServ->m_hNp, resBuf, sizeof(resBuf), &dwBytesWrite, NULL);
							}
							catch (...) {} // ������ �� ������������ ������� ����������

							::ZeroMemory(buffer, sizeof(buffer));
							::ZeroMemory(resBuf, sizeof(resBuf));
						}
					}
				}
				cServ->x_DisconnectAndClosePipe();
				success = TRUE;
			}
		}
		catch (const SocketError& ex)
		{
			eArg.message = ex.what();
			eArg.error_code = ex.GetCode();
		}
		catch (const Error& ex)
		{
			eArg.message = ex.what();
			eArg.error_code = UNKNOWN_ERROR_CODE;
		}
		catch (...)
		{
			eArg.message = UNKNOWN_ERROR_STR;
			eArg.error_code = UNKNOWN_ERROR_CODE;
		}

		cServ->x_DisconnectAndClosePipe();

		cServ->PulseEvent(SERVER_STOP_EVENT, cServ, eArg);

		::InterlockedExchange((PLONG)&cServ->m_isRun, FALSE);

		::_endthreadex(0);
		return 0;
	}

	UINT WINAPI ConsolePipe::ConnectTh(PVOID pParam)
	{
		UINT exitCode = NOERROR;
		ConsolePipe* cServ = (ConsolePipe*)(pParam);

		if (!::ConnectNamedPipe(cServ->m_hNp, NULL))
			exitCode = ::GetLastError();

		::_endthreadex(exitCode);
		return exitCode;
	}

	UINT WINAPI ConsolePipe::ReadNpTh(PVOID pParam)
	{
		UINT exitCode = NOERROR;
		DWORD dwBytesRead = 0;
		ReadNpThArg* args = (ReadNpThArg*)(pParam);

		__try {
			if (!::ReadFile(*args->hNp, args->buffer, args->bufSize, &dwBytesRead, NULL))
				exitCode = ::GetLastError();
		}
		__finally {} // ������ �� ������������ �������� ������

		::_endthreadex(exitCode);
		return exitCode;
	}

	VOID ConsolePipe::x_DisconnectAndClosePipe()
	{
		if (m_hNp != INVALID_HANDLE_VALUE)
		{
			::FlushFileBuffers(m_hNp);
			::DisconnectNamedPipe(m_hNp);
			::CloseHandle(m_hNp);
			m_hNp = INVALID_HANDLE_VALUE;
		}
	}
} // End namespace sys

#undef WAIT_TIME_MSEC
#undef CS_TIMEOUT
#undef RS_TIMEOUT 