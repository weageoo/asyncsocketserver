#include "ServerStatistics.h"

namespace sys
{
	ServerStatistics::ServerStatistics()
	{
		Clear();
	}

	VOID ServerStatistics::IncrementClient(CLIENTS_TYPE cType)
	{
		IncrementClientNoRecalc(cType);

		if (cType != CURRENT_CLIENTS)
			::InterlockedDecrement(&m_currentClients);
	}

	VOID ServerStatistics::IncrementClientNoRecalc(CLIENTS_TYPE cType)
	{
		switch (cType)
		{
		case CURRENT_CLIENTS:
			::InterlockedIncrement(&m_currentClients);
			break;
		case SUCCESS_CLIENTS:
			::InterlockedIncrement(&m_successClients);
			break;
		case VALID_CLIENTS:
			::InterlockedIncrement(&m_validClients);
		}
	}

	LONG ServerStatistics::GetNumberOfClients(CLIENTS_TYPE cType)
	{
		switch (cType)
		{
		case CURRENT_CLIENTS:
			return m_currentClients;
		case SUCCESS_CLIENTS:
			return m_successClients;
		case VALID_CLIENTS:
			return m_validClients;
		}
		return (-1);
	}

	VOID ServerStatistics::Clear()
	{
		::InterlockedExchange(&m_currentClients, 0);
		::InterlockedExchange(&m_successClients, 0);
		::InterlockedExchange(&m_validClients,   0);
	}

	std::string ServerStatistics::ToString() const
	{
		std::string str;
		str += "Connected clients: ";
		str += Convert::ToStr(m_currentClients);
		str += "\r\n";
		str += "Success clients: ";
		str += Convert::ToStr(m_successClients);
		str += "\r\n";
		str += "Valid clients: ";
		str += Convert::ToStr(m_validClients);
		return str;
	}
}