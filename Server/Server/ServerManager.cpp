#include "ServerManager.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
namespace sys
{
	ServerManager ServerManager::single_manager;

	ServerManager::ServerManager()
	{
		 m_prms.Port = 0;
		 m_prms.SessionMsec = INFINITE;
		 m_hExitEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	}
	VOID ServerManager::Load(ServerTotalParams prms)
	{
		m_aServsRun = m_rServsRun = m_cPipeRun = 0;
		x_InitServices(prms);
	}
	VOID ServerManager::LoadFromFile(LPCSTR fileName)
	{
		CMarkup xml;
		ServerTotalParams prms = {0};

		if (!xml.Load(fileName))
			throw Error("XML settings file not loaded.");
		if (!xml.IsWellFormed())
			throw Error("XML settings file not well formed.");

		if (xml.FindChildElem(XMLTAG_IP))
			prms.Ip = ::strdup(xml.GetChildData().c_str());
		if (xml.FindChildElem(XMLTAG_PORT))
			prms.Port = (USHORT)Convert::ToInt32(xml.GetChildData());
		if (xml.FindChildElem(XMLTAG_PIPENAME))
			prms.PipeName = ::strdup(xml.GetChildData().c_str());
		if (xml.FindChildElem(XMLTAG_CALLSING))
			prms.Callsing = ::strdup(xml.GetChildData().c_str());
		if (xml.FindChildElem(XMLTAG_SERVICEDLL))
			prms.ServiceDll = ::strdup(xml.GetChildData().c_str());
		if (xml.FindChildElem(XMLTAG_SMTPSERVER))
			prms.SmtpServer = ::strdup(xml.GetChildData().c_str());
		if (xml.FindChildElem(XMLTAG_ADMINEMAIL))
			prms.AdminEmail = ::strdup(xml.GetChildData().c_str());
		if (xml.FindChildElem(XMLTAG_LOGFILE))
			prms.LogFile = ::strdup(xml.GetChildData().c_str());
		if (xml.FindChildElem(XMLTAG_SESSION))
			prms.SessionMsec = (ULONG)Convert::ToInt64(xml.GetChildData());

		Load(prms);
	}
	VOID ServerManager::Start()
	{
		if (m_prms.Port != 0)
		{
			int i;
			for (i = (int)m_rServs.size(); --i >= 0; ) 
				m_rServs[i]->Start(THREAD_PRIORITY_NORMAL);
			for (i = (int)m_aServs.size(); --i >= 0; )
				m_aServs[i]->Start(THREAD_PRIORITY_NORMAL);
			m_cPipe->Start(THREAD_PRIORITY_HIGHEST);
		}
	}
	VOID ServerManager::WaitServ()
	{
		::WaitForSingleObject(m_hExitEvent, INFINITE);
	}
	VOID ServerManager::Stop()
	{
		int i = 0;
		for (i = (int)m_rServs.size(); --i >= 0; ) 
			m_rServs[i]->Stop();
		for (i = (int)m_aServs.size(); --i >= 0; )
			m_aServs[i]->Stop();
		m_cPipe->Stop();
	}
	ServerManager::~ServerManager()
	{
		int i = 0;
		for (i = (int)m_rServs.size(); --i >= 0; )
			delete m_rServs[i];
		for (i = (int)m_aServs.size(); --i >= 0; )
			delete m_aServs[i];
		delete m_cPipe;

		::CloseHandle(m_hExitEvent);
	}
	VOID ServerManager::x_WriteToLog(std::string str)
		// ����������� ������ �� ���������, ��� ��� ������� ������������ � �������� ����, 
		// ���������� ����������� �������
	{
		std::ofstream file(m_prms.LogFile, std::ios::app);
		file << str << "\r\n";
	}
	VOID ServerManager::x_SendInfoMail(std::string header, std::string message)
	{
		SmtpClient::TrySendMail(m_prms.SmtpServer, "Server", m_prms.AdminEmail, header, message);
	}
	VOID ServerManager::x_InitServices(const ServerTotalParams& prms)
	{
		ServerParams servPrms(prms.Ip, prms.Port, prms.Callsing, prms.SessionMsec);

		m_rServs.push_back(new ResponseServer);
		m_aServs.push_back(new AcceptServer(prms.ServiceDll));
		m_cPipe = new ConsolePipe;
		//-------------------------------------------------------
		m_rServs[0]->AddEventHandler(RStart, SERVER_START_EVENT);
		m_rServs[0]->AddEventHandler(RStop, SERVER_STOP_EVENT);
		m_rServs[0]->SetParams(servPrms);
		//-------------------------------------------------------
		m_aServs[0]->AddEventHandler(AStart, SERVER_START_EVENT);
		m_aServs[0]->AddEventHandler(AStop, SERVER_STOP_EVENT);
		m_aServs[0]->SetParams(servPrms);
		//-------------------------------------------------------
		m_cPipe->AddEventHandler(CPStart, SERVER_START_EVENT);
		m_cPipe->AddEventHandler(CPStop, SERVER_STOP_EVENT);
		m_cPipe->SetPipeName(prms.PipeName);
		m_cPipe->LinkService(m_rServs[0]);
		m_cPipe->LinkService(m_aServs[0]);

		m_prms = prms; // ��������� ��������� ����� �������� �������������
	}
	VOID ServerManager::x_StopEventPulse()
	{
		if (m_rServsRun > 0 || m_aServsRun > 0 || m_cPipeRun > 0);
		else
			::CloseHandle((HANDLE)::_beginthreadex(NULL, 0, xs_WaitStopThread, NULL, 0, NULL));	
	}

	UINT WINAPI ServerManager::xs_WaitStopThread(PVOID)
	{
		single_manager.m_cPipe->WaitServ();
		single_manager.m_aServs[0]->WaitServ();
		single_manager.m_rServs[0]->WaitServ();
		::SetEvent(single_manager.m_hExitEvent);
		single_manager.PulseEvent(SERVER_STOP_EVENT, &single_manager, EVENT_ARG());
		::_endthreadex(0);
		return 0;
	}

	VOID ServerManager::RStart(PVOID, EVENT_ARG)
	{
		single_manager.x_WriteToLog(x_GetNowDateTime() + " ResponceServer ��������� �������.");
		::InterlockedIncrement(&single_manager.m_rServsRun);
		single_manager.x_SendInfoMail(
			"Server:ResponceServer:Start", "ResponceServer started.");
	}
	VOID ServerManager::RStop(PVOID, EVENT_ARG e)
	{
		single_manager.x_WriteToLog(x_GetNowDateTime() + " ResponceServer �����������. �������: " + e.message);
		::InterlockedDecrement(&single_manager.m_rServsRun);
		single_manager.x_SendInfoMail(
			"Server:ResponceServer:Stop", "ResponceServer stoped. " + std::string(e.message));
		single_manager.x_StopEventPulse();
	}
	VOID ServerManager::CPStart(PVOID, EVENT_ARG)
	{
		single_manager.x_WriteToLog(x_GetNowDateTime() + " ConsolePipe ��������� �������.");
		::InterlockedIncrement(&single_manager.m_cPipeRun);
		single_manager.x_SendInfoMail(
			"Server:ConsolePipe:Start", "ConsolePipe started.");
	}
	VOID ServerManager::CPStop(PVOID, EVENT_ARG e)
	{
		single_manager.x_WriteToLog(x_GetNowDateTime() + " ConsolePipe �����������. �������: " + e.message);
		::InterlockedDecrement(&single_manager.m_cPipeRun);
		single_manager.x_SendInfoMail(
			"Server:ConsolePipe:Stop", "ConsolePipe stoped. " + std::string(e.message));
		single_manager.x_StopEventPulse();
	}
	VOID ServerManager::AStart(PVOID, EVENT_ARG)
	{
		single_manager.x_WriteToLog(x_GetNowDateTime() + " AcceptServer ��������� �������.");
		::InterlockedIncrement(&single_manager.m_aServsRun);
		single_manager.x_SendInfoMail(
			"Server:AcceptServer:Start", "AcceptServer started.");
	}
	VOID ServerManager::AStop(PVOID, EVENT_ARG e)
	{
		single_manager.x_WriteToLog(x_GetNowDateTime() + " AcceptServer �����������. �������: " + e.message);
		::InterlockedDecrement(&single_manager.m_aServsRun);
		single_manager.x_SendInfoMail(
			"Server:AcceptServer:Stop", "AcceptServer stoped. " + std::string(e.message));
		single_manager.x_StopEventPulse();
	}
}