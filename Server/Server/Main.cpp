#include "ServerManager.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <tchar.h>
#include <windows.h>
#include <iostream>
#include <conio.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
using namespace sys;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int _tmain(int argc, _TCHAR* argv[])
{
	try
	{
		LPTSTR settingsFile = "setup.xml"; // ���� �������� �� ���������
		if (argc > 1)
			settingsFile = argv[1];

		ServerManager& sManager = ServerManager::instance();

		sManager.LoadFromFile(settingsFile);
		sManager.Start();

		std::cout << "Concurrent Server 2009 v1.0. All rights reserved.\n";
		std::cout << "-------------------------------------------------\n";
		sManager.WaitServ();
		std::cout << "Shutdown application...\n";
	}
	catch (const std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
		::system("pause");
	}
	return 0;
}