#pragma once
#include "Server.h"
#include "ClientList.h"
#include "ServerStatistics.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <WinSock2.h>
#include <string>
#include <vector>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#pragma comment(lib, "Ws2_32.lib")
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	// ��� �������-����������� ��������
	typedef BOOL (*PSERVICE)(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize);

	class AcceptServer : public Server
	{
	private:
		HANDLE           m_hThread;         // ��������� ���������� ������
		HANDLE           m_hIOCP;           // ��������� ����� ����������
		HANDLE           m_hAcceptThread;   // ��������� ������ ����������� ��������
		PHANDLE          m_phWorkerThreads; // ������ ���������� ������� ������� ����� ����������
		SOCKET           m_listenSocket;    // �������������� �����
		HMODULE          m_hDllLibrary;     // ��������� dll-���������� ��������
		WSAEVENT         m_hAcceptEvent;    // ������� ������������� �������
		static INT       m_nWorkThreads;    // ���-�� ������� �������
		ClientList       m_clientList;      // ������ ������������ ��������
		volatile BOOL    m_isRun;           // ������� �� ������?
		volatile BOOL    m_isWait;          // ������ ������� ���������� ���������� �������?
		// ��������� ������� ������������������ ��������
		BOOL (*m_fProcessRequest)(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize);
	public:
		AcceptServer(LPCTSTR dllName);
		~AcceptServer();
		VOID Start(INT nPriority);
		VOID Start();
		VOID Stop(DWORD waitTime);
		VOID Stop();
		VOID Wait(); // ���������� ����������� �������� �� ��� ���, ���� �� ����� �������� ���������
		BOOL IsWait();
		VOID Shutdown();
		BOOL IsStarted() const;
		BOOL SetPriority(INT);
		INT  GetPriority() const;
		VOID WaitServ() const; // �������, ���� ������ �������
		VOID ExecuteCommand(REMOTE_COMMANDS, COMMAND_PARAMS, ANSWER&);
	private:
		// ������� ���������� ������
		static UINT WINAPI NetThread(PVOID lpParam);
		// ������� ������, ����������� �� ������������� ��������
		static UINT WINAPI AcceptThread(PVOID lpParam);
		// ������� ������ ��� ������������ ����� ����������
		static UINT WINAPI WorkerThread(PVOID lpParam);
		// ��������� ��� ���������� ������� (APC)
		static VOID CALLBACK ShutdownAP(ULONG_PTR dwParam);
		// �������� ���-�� ����������� � �������
		static INT GetNoOfProcessors();
		// ��������� ������������� �������
		VOID x_AcceptConnection(SOCKET listenSocket);
		// ������������� ����� � �������� ���������� � ������ ����������
		BOOL x_AssociateWithIOCP(ClientContext* pClientContext);
	};
}