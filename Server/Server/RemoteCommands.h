#pragma once
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <set>
#include <string>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	// ������� ����������, ���������� � �������� [��������_���������, ��������]
	typedef std::map<std::string, std::string> COMMAND_PARAMS;

	// ��������� �������� �������.
	enum REMOTE_COMMANDS
	{
		NULL_REMOTE_COMMAND,       // ������ �������
		START_REMOTE_COMMAND,      // ��������� ������
		STOP_REMOTE_COMMAND,       // ���������� ������ (� ������������ �������)
		SHUTDOWN_REMOTE_COMMAND,   // ��������� ������ (�������� ��������)
		WAIT_REMOTE_COMMAND,       // ������������� ����������� ��������
		EXIT_REMOTE_COMMAND,       // ��������� ������ ���������������
		STATISTICS_REMOTE_COMMAND  // ������ �������������� ����������
	};

	struct CmdStrPair
	{
		LPCSTR          cmdname;
		REMOTE_COMMANDS command;
	};

	static CmdStrPair CMDSTRLIST[] = 
	{
		{ "start", START_REMOTE_COMMAND           },
		{ "stop", STOP_REMOTE_COMMAND             },
		{ "wait", WAIT_REMOTE_COMMAND             },
		{ "shutdown", SHUTDOWN_REMOTE_COMMAND     },
		{ "stat", STATISTICS_REMOTE_COMMAND       },
		{ "statistics", STATISTICS_REMOTE_COMMAND },
		{ "exit", EXIT_REMOTE_COMMAND             }
	};

	#define CMDSTRLIST_SIZE sizeof(CMDSTRLIST)/sizeof(CmdStrPair)

	struct ANSWER
	{
		ANSWER() : info(""), error("") {}
		std::string info;
		std::string error;
	};

	// ��������� �������� �������.
	class IRemoteCommands
	{
	public:
		virtual VOID ExecuteCommand(REMOTE_COMMANDS, COMMAND_PARAMS, ANSWER&) = 0;
	};
}