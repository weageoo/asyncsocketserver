#pragma once
#include "Server.h"
#include "CriticalSection.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <tchar.h>
#include <windows.h>
#include <process.h>
#include <vector>
#include <list>
#include <string>
#include <aclapi.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	// ��������� ��� �������� � ����� ReadNpTh
	struct ReadNpThArg
	{
		PHANDLE hNp;       // �����
		PSTR    buffer;    // ����� ��� �����
		DWORD   bufSize;   // ������ ������
	};

	class ConsolePipe : public EventHelper
	{
	private:
		HANDLE          m_hThread;             // ��������� "������������" ������
		HANDLE          m_hNp;                 // ��������� ������
		LPCSTR          m_pipeName;            // ��� NP-������
		volatile BOOL   m_isRun;               // ������� �� ������?
		std::vector<IRemoteCommands*> m_servs; // C������, �������������� �������� ����������
	public:
		ConsolePipe();
	   ~ConsolePipe();
		VOID   Start(INT threadPriority);
	    VOID   Start();
	    VOID   Stop(DWORD waitTimeMsec);
	    VOID   Stop();
	    BOOL   IsStarted() const;
		BOOL   SetPriority(INT);
		INT    GetPriority() const;
		VOID   SetPipeName(LPCSTR pipeName);
		LPCSTR GetPipeName() const;
		VOID   LinkService(IRemoteCommands*);                   // ����� ��� �������� ������
		VOID   WaitServ() const;                                // �������, ���� ������ �� ����������
		VOID   ExecuteRemoteCommand(LPCSTR cmd, LPSTR report);  // ��������� �������� �������
	private:
		static UINT WINAPI NetThread(PVOID pParam);            // ������� ������
		static UINT WINAPI ConnectTh(PVOID pParam);            // �����, ��������� �������������
		static UINT WINAPI ReadNpTh(PVOID pParam);             // �����, ��������� ������ � �����
 		VOID x_DisconnectAndClosePipe();
	};
}