#include "AcceptServer.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <process.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define WORKER_THREADS_PER_PROCESSOR (2)     // ���-�� ������� IO ������������ ���-�� �����������
#define WAIT_TIMEOUT_INTERVAL        (300)   // �������� �������� ������� ���������� ������
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	INT AcceptServer::m_nWorkThreads = WORKER_THREADS_PER_PROCESSOR * AcceptServer::GetNoOfProcessors();

	AcceptServer::AcceptServer(LPCTSTR dllName)
	{
		m_isRun  = FALSE;
		m_isWait = FALSE;
		m_hIOCP = NULL;
		m_hDllLibrary = NULL;
		m_fProcessRequest = NULL;
		m_hThread = INVALID_HANDLE_VALUE;
		m_hAcceptThread = INVALID_HANDLE_VALUE;
		m_hAcceptEvent = INVALID_HANDLE_VALUE;
		

		if ((m_hDllLibrary = ::LoadLibrary(dllName)
			) == NULL)
			throw Error("Error occurred while LoadLibrary().");

		if ((m_fProcessRequest = (PSERVICE)::GetProcAddress(m_hDllLibrary, "ProcessRequest")
			) == NULL)
			throw Error("Error occurred while GetProcAddress().");

		m_phWorkerThreads = new HANDLE[m_nWorkThreads];
	}

	AcceptServer::~AcceptServer()
	{
		if (m_hDllLibrary != NULL)
			::FreeLibrary(m_hDllLibrary);

		delete m_phWorkerThreads;
	}

	VOID AcceptServer::Start(INT nPriority)
	{
		if (IsStarted())
			throw Error("AcceptServer::Start(): The server is already started");	

		if (m_params.IsCorrectlyInit() == FALSE)
			throw Error("AcceptServer::Start(): The server parameters incorrect");

		try
		{
			// ������������� ������������ ����� ����������� �������
			m_clientList.SetSessionTime(m_params.GetSessionTime());

			m_hThread = (HANDLE)::_beginthreadex(
				NULL, NULL, NetThread, static_cast<PVOID>(this), CREATE_SUSPENDED, NULL);
			if (m_hThread == INVALID_HANDLE_VALUE)
				throw ThreadError("The working stream of a server has not been created.");

			if (::SetThreadPriority(m_hThread, nPriority) == FALSE)
				throw ThreadError("SetThreadPriority() error.");

			m_isRun = TRUE;

			::ResumeThread(m_hThread);
		}
		catch (...)
		{
			if (m_hThread != INVALID_HANDLE_VALUE)
				this->Stop();
			throw;
		}
	}

	VOID AcceptServer::Start()
	{
		this->Start(THREAD_PRIORITY_NORMAL);
	}

	VOID AcceptServer::Stop(DWORD waitTimeMsec)
	{	
		if (IsStarted())
		{
			// ����� ����� ������ ��������� ����������
			::QueueUserAPC(ShutdownAP, m_hThread, 0); // ����������� ����� APC
			::InterlockedExchange((PLONG)&m_isRun, FALSE);

			switch (::WaitForSingleObject(m_hThread, waitTimeMsec))
			{
			case WAIT_OBJECT_0:
				::CloseHandle(m_hThread);
				break;
			case WAIT_TIMEOUT:
				::TerminateThread(m_hThread, WAIT_TIMEOUT);
				::CloseHandle(m_hThread);
				break;
			}
			m_hThread = INVALID_HANDLE_VALUE;
		}
	}

	VOID AcceptServer::Stop()
	{
		this->Stop(INFINITE);
	}

	VOID AcceptServer::Wait()
	{
		m_isWait = TRUE;
	}

	VOID AcceptServer::Shutdown()
	{
		// ��������� �����������
		::QueueUserAPC(ShutdownAP, m_hAcceptThread, 0);
		Wait(); // ������������ � wait-�����
		while (IsWait()) // ���� ��� ������ �� �����������
			::Sleep(WAIT_TIMEOUT_INTERVAL); // �������
		Stop(); // ������������� ������, ����� ��� �����������
	}

	BOOL AcceptServer::IsWait()
	{
		if (m_isWait == TRUE && !m_clientList.Count())
				m_isWait = FALSE;

		return m_isWait;
	}

	BOOL AcceptServer::IsStarted() const
	{
		return m_isRun;
	}

	VOID AcceptServer::WaitServ() const
	{
		if (m_hThread != INVALID_HANDLE_VALUE)
			::WaitForSingleObject(m_hThread, INFINITE);
	}

	VOID AcceptServer::ExecuteCommand(REMOTE_COMMANDS cmd, COMMAND_PARAMS prms, ANSWER& answer)
	{
		switch (cmd)
		{
		case START_REMOTE_COMMAND:
			if (IsStarted())
				answer.info = "AcceptServer is already started.";
			else
			{
				this->Start();
				answer.info = "AcceptServer started successfully.";
			}
			break;
		case STOP_REMOTE_COMMAND:
		case EXIT_REMOTE_COMMAND:
			this->Stop();
			answer.info = "AcceptServer stopped successfully.";
			break;
		case WAIT_REMOTE_COMMAND:
			this->Wait();
			answer.info = "AcceptServer wait.";
			break;
		case SHUTDOWN_REMOTE_COMMAND:
			this->Shutdown();
			answer.info = "AcceptServer shutdown.";
			break;
		case STATISTICS_REMOTE_COMMAND:
			answer.info = m_clientList.GetStatistics().ToString();
			break;
		}
	}

	UINT WINAPI AcceptServer::NetThread(PVOID lpParam)
	{
		AcceptServer* aServ = NULL;   // ��������� �� �����, ��������� � ������� 
		WSADATA wsaData = {0};        // ���������� � ������ ���������� �������
		SOCKADDR_IN serverAddr = {0}; // ��������� ������ �������
		EVENT_ARG eArg;               // �������� �������

		SeTranslator::SetDefaultTransFuncForCurrentThread(); // ���������� SE-����������

		try
		{
			aServ = static_cast<AcceptServer*>(lpParam);
			aServ->PulseEvent(SERVER_START_EVENT, aServ, eArg);  // ������� ������� �������

			// << ������ �������������

			if (::WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
				throw SocketError("Error occurred while WSAStartup(): ", ::WSAGetLastError());

			if ((aServ->m_listenSocket = ::WSASocket(
				AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED)
				) == INVALID_SOCKET)
				throw SocketError("Error occurred while opening socket: ", ::WSAGetLastError());

			::ZeroMemory(&serverAddr, sizeof(serverAddr));
			serverAddr = aServ->GetParams().GetSockaddr_in();

			if ((aServ->m_hIOCP = ::CreateIoCompletionPort( // ������ ���� ���������� 
				// (������� ������� - ��� �������� CP � ��� �������� ���������� � �����)
				INVALID_HANDLE_VALUE, // �����, ����������� � ������ (INVALID_HANDLE_VALUE ��� ��������)
				NULL,                 // ������������ ���� (NULL ��� �������� �����)
				0,                    // ������������� ����������
				0)                    // ����� ������� �� ���� (0 - ����� ������� ����� ����� �����������)
				) == NULL)
				throw Error("Error at creation of IOCP.");

			for (int i = aServ->m_nWorkThreads; --i >= 0; )
				aServ->m_phWorkerThreads[i] = // �������� ������� ������� �������
					(HANDLE)::_beginthreadex(NULL, NULL, WorkerThread, aServ, NULL, NULL);

			if (::bind(aServ->m_listenSocket, (PSOCKADDR)&serverAddr, sizeof(serverAddr)
				) == SOCKET_ERROR)
				throw SocketError("Error occurred while bind(): ", ::WSAGetLastError());

			if (::listen(aServ->m_listenSocket, SOMAXCONN
				) == SOCKET_ERROR)
				throw SocketError("Error occurred while listen(): ", ::WSAGetLastError());

			if ((aServ->m_hAcceptEvent = ::WSACreateEvent()
				) == WSA_INVALID_EVENT)
				throw Error("Error occurred while WSACreateEvent().");

			if (::WSAEventSelect(
				aServ->m_listenSocket, // �����, ������� �������� �� ����� �������
				aServ->m_hAcceptEvent, // ������, ��������������� ��� ������������� ������� �� ������
				FD_ACCEPT              // ��� ������� (����� - ������������ accept())
				) == SOCKET_ERROR)
				throw Error("Error occurred while WSAEventSelect().");

			aServ->m_clientList.StartGC(); // ���������� ������������ ��������

			aServ->m_hAcceptThread // ����� ��� ����������� ��������
				= (HANDLE)::_beginthreadex(0, 0, AcceptThread, static_cast<PVOID>(aServ), 0, NULL);
		
			::SleepEx(INFINITE, TRUE);
		}
		catch (const SocketError& ex)
		{
			eArg.message = ex.what();
			eArg.error_code = ex.GetCode();
		}
		catch (const std::exception& ex)
		{
			eArg.message = ex.what();
			eArg.error_code = UNKNOWN_ERROR_CODE;
		}
		catch (...)
		{
			eArg.message = UNKNOWN_ERROR_STR;
			eArg.error_code = UNKNOWN_ERROR_CODE;
		}

		if (aServ->m_hAcceptThread != INVALID_HANDLE_VALUE)
		{
			::QueueUserAPC(ShutdownAP, aServ->m_hAcceptThread, 0);
			::WaitForSingleObject(aServ->m_hAcceptThread, INFINITE);
		}

		if (aServ->m_hIOCP != INVALID_HANDLE_VALUE)
		{
			for (int i = 0; i < aServ->m_nWorkThreads; ++i)
			{
				// ���������� � ���� ����������� �����
				::PostQueuedCompletionStatus(aServ->m_hIOCP, 0, NULL, NULL);
				::CloseHandle(aServ->m_phWorkerThreads[i]);
				aServ->m_phWorkerThreads[i] = INVALID_HANDLE_VALUE;
			}
			// ������� ���������� ���� �������
			::WaitForMultipleObjects(aServ->m_nWorkThreads, aServ->m_phWorkerThreads, TRUE, INFINITE);
		}

		// ������� �����������
		if (aServ->m_hAcceptEvent != INVALID_HANDLE_VALUE)
			::WSACloseEvent(aServ->m_hAcceptEvent);
		if (aServ->m_listenSocket)
			::closesocket(aServ->m_listenSocket);
		if (aServ->m_hIOCP)
			::CloseHandle(aServ->m_hIOCP);

		if (wsaData.wVersion != NULL) 
			::WSACleanup();

		aServ->m_clientList.Clear();
		aServ->m_clientList.StopGC();

		::InterlockedExchange((PLONG)&aServ->m_isRun, FALSE);
		
		aServ->PulseEvent(SERVER_STOP_EVENT, aServ, eArg);
		
		::_endthreadex(0);
		return 0;
	}

	UINT WINAPI AcceptServer::AcceptThread(PVOID lpParam)
	{
		AcceptServer* aServ = static_cast<AcceptServer*>(lpParam);
		WSANETWORKEVENTS wsaEvents;

		while (::SleepEx(0, TRUE) == 0)
		{
			if (::WSAWaitForMultipleEvents(1, &aServ->m_hAcceptEvent, FALSE, WAIT_TIMEOUT_INTERVAL, FALSE
				) != WSA_WAIT_TIMEOUT && !aServ->IsWait())
			{
				::WSAEnumNetworkEvents(aServ->m_listenSocket, aServ->m_hAcceptEvent, &wsaEvents);
				if (!aServ->IsWait() && (wsaEvents.lNetworkEvents & FD_ACCEPT) 
					&& (wsaEvents.iErrorCode[FD_ACCEPT_BIT] == 0))
				{
					aServ->x_AcceptConnection(aServ->m_listenSocket);
				}
			}
		}
		return 0;
	}

	UINT WINAPI AcceptServer::WorkerThread(PVOID lpParam)
	{
		AcceptServer* aServ = static_cast<AcceptServer*>(lpParam);

		BOOL isWork = TRUE;
		PVOID lpContext = NULL;
		LPOVERLAPPED pOverlapped = NULL;
		ClientContext *pClientContext = NULL;
		DWORD dwBytesTransfered = 0;
		INT nBytesRecv = 0;
		INT nBytesSent = 0;
		DWORD dwBytes = 0, dwFlags = 0;
		CHAR szBuffer[MAX_BUFFER_LEN];

		while ( (aServ->IsStarted()) && (isWork) )
		{
			// ������� ���������� ��������������� �������� �����-������
			BOOL bReturn = ::GetQueuedCompletionStatus(
				aServ->m_hIOCP,       // ���� ����������
				&dwBytesTransfered,   // ���� ��������
				(LPDWORD)&lpContext,  // �������� ����������
				&pOverlapped,
				INFINITE);

			if ( isWork = (lpContext != NULL) )
			{
				pClientContext = (ClientContext*)lpContext;
				pClientContext->Update(); // ���������������� ��������� �������

				if ((bReturn == FALSE) || ((bReturn == TRUE) && (dwBytesTransfered == 0)))
				{
					// ���������� � ������� ���������...
					aServ->m_clientList.Remove(pClientContext, ServerStatistics::VALID_CLIENTS);
				}
				else
				{
					LPWSABUF p_wbuf = pClientContext->GetWSABUFPtr();
					LPOVERLAPPED p_ol = pClientContext->GetOVERLAPPEDPtr();

					switch (pClientContext->GetOpCode())
					{
					case ClientContext::OP_READ: // ���������� WSASend

						pClientContext->IncrSentBytes(dwBytesTransfered);

						// << �������� ������ ������� ���������.
						// << ���������, ��� �� ������ ���� �������� �� �������.
						// << ���� �������� �� ��� ������, �� ������ ��������� "������".

						if (pClientContext->GetSentBytes() < pClientContext->GetTotalBytes())
						{
							pClientContext->SetOpCode(ClientContext::OP_READ);

							p_wbuf->buf += pClientContext->GetSentBytes();
							p_wbuf->len = pClientContext->GetTotalBytes() - pClientContext->GetSentBytes();

							dwFlags = 0;

							nBytesSent = ::WSASend(pClientContext->GetSocket(), p_wbuf, 1, 
								&dwBytes, dwFlags, p_ol, NULL);

							if ((nBytesSent == SOCKET_ERROR) && (::WSAGetLastError() != WSA_IO_PENDING))
								aServ->m_clientList.Remove(pClientContext, ServerStatistics::VALID_CLIENTS);
						}
						else // ��� ������ ������� �������
						{
							pClientContext->SetOpCode(ClientContext::OP_WRITE);
							pClientContext->ResetWSABUF();

							dwFlags = 0;

							nBytesRecv = ::WSARecv(pClientContext->GetSocket(), p_wbuf, 1, 
								&dwBytes, &dwFlags, p_ol, NULL);

							if ((nBytesRecv == SOCKET_ERROR) && (::WSAGetLastError() != WSA_IO_PENDING))
								aServ->m_clientList.Remove(pClientContext, ServerStatistics::VALID_CLIENTS);
						}

						break;

					case ClientContext::OP_WRITE: // ���������� WSARecv

						pClientContext->GetBuffer(szBuffer);

						// ��������� ��������� �� dll
						if ( aServ->m_fProcessRequest(szBuffer, p_wbuf, MAX_BUFFER_LEN) )
						{
							pClientContext->SetOpCode(ClientContext::OP_READ);
							pClientContext->SetTotalBytes(p_wbuf->len);
							pClientContext->SetSentBytes(0);

							dwFlags = 0;

							nBytesSent = ::WSASend(
								pClientContext->GetSocket(), p_wbuf, 1, &dwBytes, dwFlags, p_ol, NULL);

							if ((nBytesSent == SOCKET_ERROR) && (::WSAGetLastError() != WSA_IO_PENDING))
								aServ->m_clientList.Remove(pClientContext, ServerStatistics::VALID_CLIENTS);
						}
						else // �������-���������� ������� FALSE - ������� ����� ���������
							aServ->m_clientList.Remove(pClientContext, ServerStatistics::SUCCESS_CLIENTS);
						break;

					default:
						break;
					} // End switch
				} // End if ((bReturn == FALSE) || ((bReturn == TRUE) && (dwBytesTransfered == 0)))
			} // End if ( isWork == (lpContext != NULL) )
		} // End while

		return 0;
	}

	VOID CALLBACK AcceptServer::ShutdownAP(ULONG_PTR dwParam)
	{
		// ��� ������� �������� �����, � ������� ��� ��������.
		if (dwParam)
			throw; // ���� dwParam > 0, �� ���������� ������������ ���������� ����������
	}

	VOID AcceptServer::x_AcceptConnection(SOCKET listenSocket)
	{
		SOCKADDR_IN clientAddress;
		INT nClientLength = sizeof(clientAddress);

		SOCKET clientSock = ::accept(listenSocket, (PSOCKADDR)&clientAddress, &nClientLength);

		if (clientSock == INVALID_SOCKET)
			throw SocketError("Error occurred while accepting socket: ", ::WSAGetLastError());

		// ������� ����� ClientContext ��� ������ ����������������� �������.
		ClientContext *pClientContext = new ClientContext;
		pClientContext->SetOpCode(ClientContext::OP_WRITE);
		pClientContext->SetSocket(clientSock);

		if (x_AssociateWithIOCP(pClientContext))
		{
			WSABUF     *pWbuf = pClientContext->GetWSABUFPtr();
			OVERLAPPED *pOl   = pClientContext->GetOVERLAPPEDPtr();

			DWORD dwFlags = 0;
			DWORD dwBytes = 0;

			m_clientList.Add(pClientContext); // ���������� �������

			// ������� ������ �� �������.
			// ����� WSARecv() � ������� ������ ������ �� �������� ����������������.
			INT nBytesRecv = WSARecv(pClientContext->GetSocket(), pWbuf, 1, 
				&dwBytes, &dwFlags, pOl, NULL);

			if ((nBytesRecv == SOCKET_ERROR) && (::WSAGetLastError() != WSA_IO_PENDING))
				m_clientList.Remove(pClientContext, ServerStatistics::VALID_CLIENTS);
		}
	}

	BOOL AcceptServer::x_AssociateWithIOCP(ClientContext* pClientContext)
	{
		// ����������� ����� � ������ ����������
		HANDLE hTemp = ::CreateIoCompletionPort(
			(HANDLE)pClientContext->GetSocket(), m_hIOCP, (DWORD)pClientContext, 0);

		if (hTemp == NULL)
		{
			m_clientList.Remove(pClientContext, ServerStatistics::VALID_CLIENTS);
			return FALSE;
		}
		return TRUE;
	}

	INT AcceptServer::GetNoOfProcessors()
	{
		static int nProcessors = 0;

		if (nProcessors == 0)
		{
			SYSTEM_INFO sysInfo;
			::GetSystemInfo(&sysInfo);
			nProcessors = sysInfo.dwNumberOfProcessors;
		}

		return nProcessors;
	}
} // End namespace sys

#undef WORKER_THREADS_PER_PROCESSOR
#undef WAIT_TIMEOUT_INTERVAL