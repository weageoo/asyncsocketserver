#pragma once
#include <vector>
#include <string>
#include <winsock2.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const INT MAX_BUFFER_LEN = 1024; // ������������ ������ ������� �� �������
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class ClientContext  // ���������� ��� ������������ ������� (�������� ����������)
	{
	public:
		enum OPCODES // ��������� ���� ����������� ��������
		{
			OP_NONE, OP_READ, OP_WRITE
		};
	private:
		LPOVERLAPPED      m_pol;           // ��������� ����������, ������������ � ���������� �����/������
		LPWSABUF          m_pwbuf;         // ����� ������

		INT               m_nTotalBytes;   // ����� ����
		INT               m_nSentBytes;    // ����� ���� �������

		SOCKET            m_Socket;        // ����� ��� ����� � ��������
		OPCODES           m_nOpCode;       // ��� �������� �� ������ (������������ ������� �������)

		DWORD             m_lastQuery;     // ����� ���������� ������� �������, ����

		CHAR              m_szBuffer[MAX_BUFFER_LEN];
	public:
		ClientContext();
		~ClientContext();
		VOID   SetOpCode(OPCODES code);
		INT    GetOpCode();
		VOID   SetTotalBytes(INT n);
		INT    GetTotalBytes();
		VOID   SetSentBytes(INT n);
		VOID   IncrSentBytes(INT n);
		INT    GetSentBytes();
		VOID   SetSocket(SOCKET s);
		SOCKET GetSocket();
		VOID   SetBuffer(PSTR szBuffer);
		VOID   GetBuffer(PSTR szBuffer);
		VOID   ZeroBuffer();
		VOID   SetWSABUFLength(INT nLength);
		INT    GetWSABUFLength();
		DWORD  GetLastQueryTime();
		VOID   Update();
		VOID   Disconnect();
		LPWSABUF     GetWSABUFPtr();
		LPOVERLAPPED GetOVERLAPPEDPtr();
		VOID ResetWSABUF();
	};
}