#include "Events.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	EventArray::EventArray(){}

	VOID EventArray::Add(EVENT_HANDLER eHandler, EVENT_TYPE eType)
	{
		std::multimap<EVENT_TYPE, EVENT_HANDLER>::iterator pos;
		for(pos = m_events.begin(); pos != m_events.end(); ++pos)
		{
			if (pos->first == eType && pos->second == eHandler)
				return; // ��������� ������� ���������������� ��� �� ���������� �� �� �� �������
		}
		m_events.insert(std::make_pair(eType, eHandler));
	}
	VOID EventArray::Remove(EVENT_HANDLER eHandler, EVENT_TYPE eType)
	{
		std::multimap<EVENT_TYPE, EVENT_HANDLER>::iterator pos;
		for(pos = m_events.begin(); pos != m_events.end(); ++pos)
		{
			if (pos->first == eType && pos->second == eHandler) {
				m_events.erase(pos);
				return;
			}
		}
	}
	VOID EventArray::Pulse(EVENT_TYPE eType, PVOID sender, EVENT_ARG eArg) const
	{
		std::multimap<EVENT_TYPE, EVENT_HANDLER>::const_iterator pos;
		for(pos = m_events.begin(); pos != m_events.end(); ++pos)
		{
			if (pos->first == eType)
				pos->second(sender, eArg);
		}
	}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	CriticalSection EventHelper::m_cs; // ����������� ������, �������������� ����������� ���������� ������������ �������

	VOID EventHelper::AddEventHandler(EVENT_HANDLER eHandler, EVENT_TYPE eType)
	{
		m_cs.Enter();
		m_events.Add(eHandler, eType);
		m_cs.Leave();
	}
	VOID EventHelper::RemoveEventHandler(EVENT_HANDLER eHandler, EVENT_TYPE eType)
	{
		m_cs.Enter();
		m_events.Remove(eHandler, eType);
		m_cs.Leave();
	}
	VOID EventHelper::PulseEvent(EVENT_TYPE eType, PVOID sender, EVENT_ARG eArg)
	{
		m_cs.Enter();
		m_events.Pulse(eType, sender, eArg);
		m_cs.Leave();
	}
}