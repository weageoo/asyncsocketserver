#pragma once
#include <string>

namespace sys
{
	class MailMessage
	{
	private:
		std::string m_fromMail;
		std::string m_toMail;
		std::string m_subject;
		std::string m_body;
		//std::string m_contentType; // text/html
	public:
		MailMessage();
		MailMessage(std::string from, std::string to);
		MailMessage(std::string from, std::string to, std::string subject, std::string body);
		std::string GetFromMail() const      { return m_fromMail;   }
		void SetFromMail(std::string from)   { m_fromMail = from;   }
		std::string GetToMail() const        { return m_toMail;     }
		void SetToMail(std::string to)       { m_toMail = to;       }
		std::string GetSubject() const       { return m_subject;    }
		void SetSubject(std::string subject) { m_subject = subject; }
		std::string GetBody() const          { return m_body;       }
		void SetBody(std::string body)       { m_body = body;       }
	};
}