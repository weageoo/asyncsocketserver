#pragma once
#include "Markup.h"
#include "Events.h"
#include "SmtpClient.h"
#include "AcceptServer.h"
#include "ResponseServer.h"
#include "ConsolePipe.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const TCHAR XMLTAG_IP[]         = _TEXT("IP");
static const TCHAR XMLTAG_PORT[]       = _TEXT("PORT");
static const TCHAR XMLTAG_PIPENAME[]   = _TEXT("PIPE_NAME");
static const TCHAR XMLTAG_CALLSING[]   = _TEXT("CALLSING");
static const TCHAR XMLTAG_SERVICEDLL[] = _TEXT("SERVICE_DLL");
static const TCHAR XMLTAG_SMTPSERVER[] = _TEXT("SMTP_SERVER");
static const TCHAR XMLTAG_ADMINEMAIL[] = _TEXT("ADMIN_EMAIL");
static const TCHAR XMLTAG_LOGFILE[]    = _TEXT("LOG_FILE");
static const TCHAR XMLTAG_SESSION[]    = _TEXT("SESSION");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
namespace sys
{
	struct ServerTotalParams
	{
		LPCSTR Ip;           // IP ����-����������
		USHORT Port;         // ���� (TCP/UDP) �������
		LPCSTR PipeName;     // ��� ������ ��� ��������� ����������
		LPCSTR Callsing;     // �������� �������
		LPCSTR ServiceDll;   // ������������ Dll-���������� � ���������-���������
		LPCSTR SmtpServer;   // DNS/NetBIOS ��� ����� � SMTP-��������
		LPCSTR AdminEmail;   // ����������� ����� �������������� �������
		LPCSTR LogFile;      // ���� ��� ������ ��������� ���������
		ULONG  SessionMsec;  // ������������ ����� ����������� �������, ����
	};

	class ServerManager : public EventHelper
	{	
	private:
		static ServerManager single_manager;

		std::vector<AcceptServer*>   m_aServs;
		std::vector<ResponseServer*> m_rServs;
		ConsolePipe*                 m_cPipe;
		ServerTotalParams            m_prms;
		volatile LONG                m_aServsRun, m_rServsRun, m_cPipeRun;
		HANDLE                       m_hExitEvent; // ������� ���������� �������

		ServerManager();
		ServerManager& operator=(ServerManager&); // �� �������������!
		ServerManager(const ServerManager&);      // �� �������������!
		~ServerManager();

	public:
		static ServerManager& instance() { return single_manager; }
		const ServerTotalParams& GetParams() const { return m_prms; }
		ConsolePipe& GetConsole() const { return *m_cPipe; }
		VOID Load(ServerTotalParams prms);
		VOID LoadFromFile(LPCSTR fileName);
		VOID Start();
		VOID Stop();
		VOID WaitServ();

	private:
		VOID x_WriteToLog(std::string str);
		VOID x_InitServices(const ServerTotalParams& prms);
		VOID x_SendInfoMail(std::string header, std::string message);
		VOID x_StopEventPulse();

		static std::string x_GetNowDateTime()
		{
			TCHAR time[15];
			SYSTEMTIME sysTime;

			::GetLocalTime(&sysTime);
			::GetTimeFormat(LOCALE_SYSTEM_DEFAULT, TIME_FORCE24HOURFORMAT, &sysTime,
				TEXT("HH':'mm':'ss tt"), time, sizeof(time));

			return std::string(time);
		}

		/// <-- ����������� �������
			
		static VOID RStart(PVOID, EVENT_ARG);
		static VOID RStop(PVOID, EVENT_ARG);
		static VOID CPStart(PVOID, EVENT_ARG);
		static VOID CPStop(PVOID, EVENT_ARG);
		static VOID AStart(PVOID, EVENT_ARG);
		static VOID AStop(PVOID, EVENT_ARG);

		/// -->

		// ������� ������, ��������� ������� ���������� ���� ��������
		static UINT WINAPI xs_WaitStopThread(PVOID);
	};
}