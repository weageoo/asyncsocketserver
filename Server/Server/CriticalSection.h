#pragma once
#include "Exceptions.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <Windows.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class CriticalSection
	{
	private:
		CRITICAL_SECTION m_cs;
	public:
		CriticalSection();
		~CriticalSection();
		VOID Enter();    // ����� � ����������� ������
		BOOL TryEnter(); // ����� � ����������� ������, ���� �� �� ������� ������ ������
		VOID Leave();    // �������� ����������� ������
	};
}