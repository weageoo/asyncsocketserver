#pragma once
#include "Socket.h"
#include "Exceptions.h"
#include "MailMessage.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <string>
#include <WinSock2.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class SmtpClient
	{
	private:
		CSocket     m_sock;
		std::string m_host;
		std::string m_lastError;
		u_short     m_port;
		bool        m_isConnect;
	public:
		SmtpClient() : m_port(0), m_isConnect(false) {}
		~SmtpClient() {}
		bool Connect(std::string server, u_short port = 0);
		bool Disconnect();
		std::string SmtpClient::GetHost() const      { return m_host;      }
		u_short SmtpClient::GetPort() const          { return m_port;      }
		std::string SmtpClient::GetLastError() const { return m_lastError; }
		void SmtpClient::SetHost(std::string host)   { m_host = host;      }
		void SetPort(u_short port)                   { m_port = port;      }
		bool Send(MailMessage msg);
		bool Send(std::string from, std::string to, std::string subject, std::string body);
		// static
		static bool TrySendMail(
			std::string smtpServer, std::string from, std::string to, std::string subject, std::string body)
		{
			SmtpClient smtp;
			if (!smtp.Connect(smtpServer) || !smtp.Send(from, to, subject, body)) 
				return false;
			smtp.Disconnect();
			return true;
		}
	private:
		int x_GetSmtpResponseCode(std::string str)
		{
			return ::atoi(str.substr(0, 3).c_str());
		}
		int x_SmtpSendRecv(std::string msg)
		{
			return x_GetSmtpResponseCode(m_sock.SendRecv(msg));			
		}
		std::string x_GetDateTime() const;
	};
}
/* Sample using SmtpClient class:
 * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * SmtpClient smtp;
 * if (!smtp.Connect("virt", 25))
 *	 cout << smtp.GetLastError() << endl;
 * else if (!smtp.Send("lol@mail.ru", "admin@aad.net",
 *	 "Subject.", "This message body."))
 *	 cout << smtp.GetLastError() << endl;
 * else if (!smtp.Disconnect()) 
 *	 cout << smtp.GetLastError() << endl;
 * * * * * * * * * * * * * * * * * * * * * * * * * * */