#pragma once 
#include "Exceptions.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class ServerParams
	{
	private:
		LPCSTR m_callsing;
		LPCSTR m_ip;
		USHORT m_port;
		ULONG  m_session;
	public:
		ServerParams(LPCSTR ip, USHORT port, LPCSTR callsing, ULONG sessionTimeMsec);
		ServerParams(USHORT port, LPCSTR callsing, ULONG sessionTimeMsec);
		ServerParams();
		BOOL IsCorrectlyInit();
		SOCKADDR_IN GetSockaddr_in() const;
		VOID SetCallsing(LPCSTR callsing);
		VOID SetIP(LPCSTR ip);
		VOID SetPort(USHORT port);
		VOID SetSessionTime(ULONG sessionTimeMsec);
		LPCSTR GetCallsing() const;
		LPCSTR GetIP() const;
		USHORT GetPort() const;
		ULONG  GetSessionTime() const;
	};
}
