#include "SmtpClient.h"

namespace sys
{
	bool SmtpClient::Send(MailMessage message)
	{
		return false;
	}
	bool SmtpClient::Send(std::string from, std::string to, std::string subject, std::string body)
	{
		try
		{
			if (from.length() <= 0)
				throw Error("Please specify a sender email address.");
			if (to.length() <= 0)
				throw Error("Please specify a recipient email address.");
			if (subject.length() <= 0)
				throw Error("Please enter subject.");

			std::string email =
				+ "From: <" + from
				+ ">\r\nTo: <" + to
				+ ">\r\nSubject: " + subject
				+ "\r\nDate: " + x_GetDateTime()
				+ "\r\nMIME-Version: 1.0" \
				  "\r\nContent-type: text/plain; charset=\"US-ASCII\"" \
				  "\r\nX-Mailer: C++ Mailer" \
				  "\r\n" + body + "\r\n";

			// ������� MAIL
			if (x_SmtpSendRecv("MAIL FROM: <" + from + ">\r\n") != 250)
				throw Error("FROM email considered bad by server.");

			// ������� RCPT
			switch (x_SmtpSendRecv("RCPT TO: <" + to + ">\r\n"))
			{
			case 250:
			case 251:
				break;
			default: 
				throw Error("TO email considered bad by server.");
			}

			// ������� DATA
			if (x_SmtpSendRecv("DATA\r\n") != 354)
				throw Error("DATA command not accepted.");

			// �������� ������
			m_sock.Send(email);

			// ������ ���������
			if (x_SmtpSendRecv("\r\n.\r\n") != 250)
				throw Error("Email not accepted.");

			return true;
		}
		catch (const std::exception& ex)
		{
			m_lastError = ex.what();
		}
		return false;
	}
	bool SmtpClient::Connect(std::string server, u_short port)
	{
		try
		{
			if (m_isConnect)
				Disconnect();

			PHOSTENT pHostEnt;
			PSERVENT pServEnt;

			m_sock.InitSocket(AddressFamily::InterNetwork, SocketType::Stream, ProtocolType::TCP);

			if (pHostEnt = ::gethostbyname(server.c_str()))
			{
				if (port == 0)
				{
					pServEnt = ::getservbyname("mail", 0);
					port = (pServEnt == NULL) ?
						IPPORT_SMTP : ntohs(pServEnt->s_port);
				}
				m_sock.Connect(*( (PIN_ADDR)*pHostEnt->h_addr_list ), port);
				if (x_GetSmtpResponseCode(m_sock.Recv()) != 220)
					throw Error("No smtp server at specified address or smtp server not ready.");

				// ������� HELO
				if (x_SmtpSendRecv("HELO\r\n") != 250)
					throw Error("HELO fails.");

				m_isConnect = true;
				m_host      = server;
				m_port      = port;

				return true;
			}
		}
		catch (const std::exception& ex)
		{
			m_lastError = ex.what();
		}
		return false;
	}
	bool SmtpClient::Disconnect()
	{
		if (!m_isConnect)
			return false;

		try
		{
			// ������� QUIT
			m_sock.SendRecv("QUIT\r\n");

			m_sock.Close();

			m_isConnect = false;

			return true;
		}
		catch (const std::exception& ex)
		{
			m_lastError = ex.what();
		}

		return false;
	}
	std::string SmtpClient::x_GetDateTime() const
	{
		TCHAR date[100], time[100];
		SYSTEMTIME sysTime = {0};

		::GetSystemTime(&sysTime);
		::GetDateFormat(LOCALE_SYSTEM_DEFAULT, 0, &sysTime, 
			TEXT("ddd',' dd MMM yyyy"), 
			date, sizeof(date));
		::GetTimeFormat(LOCALE_SYSTEM_DEFAULT, TIME_FORCE24HOURFORMAT, &sysTime, 
			TEXT("HH':'mm':'ss tt"), 
			time, sizeof(time));

		return (date + std::string(" ") + time);
	}
}