#pragma once
#include "Events.h"
#include "Exceptions.h"
#include "Converter.h"
#include "ServerParams.h"
#include "RemoteCommands.h"
#include "CriticalSection.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <WinSock2.h>

namespace sys
{
	// ����������� ������.
	class Server : public EventHelper, public IRemoteCommands
	{
	protected:
		ServerParams m_params;           // ��������� ������� (ip, ����, ��������)
	public:
		// ���������� true, ���� ������ ��� �������, � false - ���� �� ���������� ��� �� ����������.
		virtual BOOL IsStarted() const = 0;
		// ����������� ������ � �������� �����������.
		virtual VOID Start(INT nPriority) = 0;
		// ���������� ����������� ������.
		virtual VOID Start() = 0;
		// ��������� ������� � ��������� ������� ��������.
		virtual VOID Stop(DWORD waitTimeMsec) = 0;
		// ���������� ��������� (� ��������� ���������� �������).
		virtual VOID Stop() = 0;
		// ���������� ��������� �������.
		virtual VOID SetParams(ServerParams);
		// �������� ��������� �������.
		virtual ServerParams GetParams() const;
	};
}