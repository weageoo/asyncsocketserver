#pragma once
#include "Exceptions.h"
//#include <MswSock.h>   // Microsoft-specific extensions to the Windows Sockets API
#include <WinSock2.h>

namespace sys
{
	struct AddressFamily
	{
		enum Enum
		{
			InterNetwork   = PF_INET,
			InterNetworkV6 = PF_INET6,
			Unknown
		};
	};
	struct SocketType
	{
		enum Enum
		{
			Stream = SOCK_STREAM, // Use TCP
			Dgram  = SOCK_DGRAM   // Use UDP
		};
	};
	struct ProtocolType
	{
		enum Enum
		{
			IP  = IPPROTO_IP,
			TCP = IPPROTO_TCP, 
			UDP = IPPROTO_UDP,
			Unknown
		};
	};
	struct SocketMode
	{
		enum Enum
		{
			Blocking    = 0,
			NonBlocking = 1
		};
	};
	struct SocketShutdown
	{
		enum Enum
		{
			Receive = SD_RECEIVE, // Disables a socket for receiving
			Send    = SD_SEND,    // Disables a socket for sending
			Both    = SD_BOTH     // Disables a socket for both sending and receiving
		};
	};
	class IPAddress
	{
		static in_addr Parse(std::string ip)
		{
			in_addr addr;
			addr.s_addr = ::inet_addr(ip.c_str());
			return addr;
		}
	};
	class CSocket
	{

	private:
		SOCKET               m_sock;
		SocketMode::Enum     m_mode;
	public:
		CSocket();
		CSocket(AddressFamily::Enum addressFamily,
			SocketType::Enum sockeType, ProtocolType::Enum protocolType);
		virtual ~CSocket();
		void InitSocket(AddressFamily::Enum addressFamily,
			SocketType::Enum sockeType, ProtocolType::Enum protocolType);
		// Socket functional
		void        Connect(in_addr hostIp, u_short hostPort);
		void        Connect(std::string hostName, u_short hostPort);		
		//void        Disconnect(bool reuseSocket);
		void        Bind();
		void        Listen();
		CSocket     Accept();
		void        Send(std::string msg);
		void        Send(const char* const msg);
		void        Send(const char* const msg, int msgLength);
		std::string Recv();
		std::string SendRecv(std::string msg);
		std::string SendRecv(const char* const msg);
		std::string SendRecv(const char* const msg, int msgLength);
		void        Shutdown(SocketShutdown::Enum how);
		void        Close();
		// Get-Set-Is
		void             SetMode(SocketMode::Enum mode);
		SocketMode::Enum GetMode() const;
	private:
		void x_InitWinSock();
	};
}