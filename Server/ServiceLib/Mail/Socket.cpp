#include "Socket.h"

namespace sys
{
	CSocket::CSocket()
	{
		x_InitWinSock();
	}
	CSocket::CSocket(AddressFamily::Enum addressFamily, SocketType::Enum socketType, ProtocolType::Enum protocolType)
	{		
		x_InitWinSock();
		InitSocket(addressFamily, socketType, protocolType);
	}
	CSocket::~CSocket()
	{
		::WSACleanup();
	}
	void CSocket::InitSocket(AddressFamily::Enum addressFamily, SocketType::Enum sockeType, ProtocolType::Enum protocolType)
	{
		m_sock = ::socket(addressFamily, sockeType, protocolType);
		if (m_sock == INVALID_SOCKET)
			throw SocketError("Can't create socket ", ::WSAGetLastError());
		m_mode = SocketMode::Blocking;
	}
	void CSocket::Connect(in_addr hostIp, u_short hostPort)
	{
		SOCKADDR_IN sockAddr;
		sockAddr.sin_family = AF_INET;
		sockAddr.sin_addr   = hostIp;
		sockAddr.sin_port   = ::htons(hostPort);

		if (::connect(m_sock, (PSOCKADDR)&sockAddr, sizeof(sockAddr)) == SOCKET_ERROR)
			throw SocketError("Can't connect ", ::WSAGetLastError());
	}
	void CSocket::Connect(std::string hostName, u_short hostPort)
	{
		PHOSTENT pHostEnt = ::gethostbyname(hostName.c_str());
		if (pHostEnt != NULL)
			Connect(*( (PIN_ADDR)*pHostEnt->h_addr_list), hostPort);
		else
			throw Error("Can't find server.");
	}
	//void CSocket::Disconnect(bool reuseSocket)
	//{
	//	//if (!::DisconnectEx(m_sock, NULL,
	//	//	(reuseSocket) ? (TF_REUSE_SOCKET) : (TF_DISCONNECT), 0))
	//	//	throw SocketError("DisconnectEx error ", ::WSAGetLastError());
	//}
	void CSocket::Send(std::string msg)
	{
		Send(msg.c_str(), msg.length());
	}
	void CSocket::Send(const char* const msg)
	{
		Send(msg, ::strlen(msg));
	}
	void CSocket::Send(const char* const msg, int msgLength)
	{
		if (msgLength < 0)
			throw Error("Incorrect message length.");
		if (::send(m_sock, msg, msgLength, 0) == SOCKET_ERROR)
			throw SocketError("Socket send error: ", ::WSAGetLastError());
	}
	std::string CSocket::Recv()
	{
		char inBuf[4096];
		if (::recv(m_sock, inBuf, sizeof(inBuf), 0) < 0)
			throw SocketError("Socket recv error: ", ::WSAGetLastError());
		return inBuf;
	}
	std::string CSocket::SendRecv(std::string msg)
	{
		return SendRecv(msg.c_str(), msg.length());
	}
	std::string CSocket::SendRecv(const char* const msg)
	{
		return SendRecv(msg, ::strlen(msg));
	}
	std::string CSocket::SendRecv(const char* const msg, int msgLength)
	{
		Send(msg, msgLength);
		return Recv();
	}
	void CSocket::SetMode(SocketMode::Enum mode)
	{
		ULONG ulBlock = mode; // ������������� ����� (�������� ������� FIONBIO)
		if (::ioctlsocket(m_sock, FIONBIO, &ulBlock) == SOCKET_ERROR)
			throw SocketError("Error occurred while ioctlsocket(): ", ::WSAGetLastError());
		m_mode = mode;
	}
	void CSocket::Shutdown(SocketShutdown::Enum how)
	{
		::shutdown(m_sock, how);
	}
	void CSocket::Close()
	{
		::closesocket(m_sock);
	}
	SocketMode::Enum CSocket::GetMode() const
	{
		return m_mode;
	}
	void CSocket::x_InitWinSock()
	{
		WSADATA wsaData;

		if (::WSAStartup(0x0202, &wsaData))
			throw SocketError("WSAStartup: ", ::WSAGetLastError());

		if (wsaData.wVersion != 0x0202)
		{
			::WSACleanup();
			throw SocketError("Can't find a useable WinSock DLL");
		}
	}
}