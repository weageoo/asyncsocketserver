#pragma once
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include <exception>
#include <string>
#include <WinSock2.h>
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define UNKNOWN_ERROR_CODE (-1)
#define UNKNOWN_ERROR_STR  ("Unknown Error")
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace sys
{
	class Error : public std::exception
	{
	protected:
		std::string m_msg;
	public:
		Error();
		Error(const char* msg);
		const char* GetMessage() const;
		virtual const char* what() const throw();
	};

	class ThreadError : public Error
	{
	public:
		ThreadError();
		ThreadError(const char* msg);
	};

	class SocketError : public Error
	{
	protected:
		int m_errorCode;
	public:
		SocketError();
		SocketError(const char* msg);
		SocketError(int code);
		SocketError(const char* msg, int code);
		int GetCode() const;
		static const char* GetSocketErrorMessage(int code);
	};
}