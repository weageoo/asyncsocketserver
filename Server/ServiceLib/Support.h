#pragma once
#include "Markup.h"
#include <tchar.h>
#include <WinSock2.h>
#pragma comment(lib, "Ws2_32.lib")
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const TCHAR ADMIN_EMAIL[] = "sasha@aad.net";
static const TCHAR SMTP_SERVER[] = "virt";
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const TCHAR XMLTAG_PARAMS[]      = _TEXT("PARAMS");
static const TCHAR XMLTAG_RESPONSE[]    = _TEXT("RESPONSE");
static const TCHAR XMLTAG_REQUEST[]     = _TEXT("REQUEST");
static const TCHAR XMLTAG_SERVICE[]     = _TEXT("SERVICE");
static const TCHAR XMLTAG_RESULT[]      = _TEXT("RESULT");
static const TCHAR XMLTAG_ERRORS[]      = _TEXT("ERRORS");
static const TCHAR XMLTAG_ERROR[]       = _TEXT("ERROR");
static const TCHAR XMLTAG_MAILER[]      = _TEXT("MAILER");
static const TCHAR XMLTAG_SUBJECT[]     = _TEXT("SUBJECT");
static const TCHAR XMLTAG_MSG[]         = _TEXT("MSG");
static const TCHAR XMLTAG_MIN[]         = _TEXT("MIN");
static const TCHAR XMLTAG_MAX[]         = _TEXT("MAX");
static const TCHAR XMLTAG_NUM[]         = _TEXT("NUM");
static const TCHAR XMLTAG_TIMEZONE[]    = _TEXT("TIMEZONE");
static const TCHAR XMLTAG_YEAR[]        = _TEXT("YEAR");
static const TCHAR XMLTAG_MONTH[]       = _TEXT("MONTH");
static const TCHAR XMLTAG_DAY_OF_WEEK[] = _TEXT("DAY_OF_WEEK");
static const TCHAR XMLTAG_DAY[]         = _TEXT("DAY");
static const TCHAR XMLTAG_HOUR[]        = _TEXT("HOUR");
static const TCHAR XMLTAG_MINUTE[]      = _TEXT("MINUTE");
static const TCHAR XMLTAG_SECOND[]      = _TEXT("SECOND");

static const TCHAR ECHO_SERV_NAME[]     = _TEXT("echo");
static const TCHAR RAND_SERV_NAME[]     = _TEXT("rand");
static const TCHAR TIME_SERV_NAME[]     = _TEXT("time");
static const TCHAR POST_SERV_NAME[]     = _TEXT("post");
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const TCHAR ERR_NOT_CONTAIN_MAILER[] = "100 The inquiry does not contain mailer.";
static const TCHAR ERR_NOT_CONTAIN_HEADER[] = "101 The inquiry does not contain head.";
static const TCHAR ERR_NOT_CONTAIN_BODY[]   = "102 The inquiry does not contain body.";
static const TCHAR ERR_NOT_CONTAIN_PARAMS[] = "103 The inquiry does not contain parametres.";
static const TCHAR ERR_NOT_SEND_EMAIL[]     = "104 Email not send. SMTP error.";
static const TCHAR ERR_NOT_SUPPORT_SERV[]   = "105 Service not find or not support.";
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static const TCHAR INF_SUCCESS_SENT[] = "+OK The message is successfully sent.";
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~