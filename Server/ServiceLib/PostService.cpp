#include "PostService.h"

BOOL PostService(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize)
{
	CMarkup xml(inBuf);              // ������� XML
	std::vector<std::string> errors; // ������ �������
	std::string mlr, hdr, msg;       // �����������(mlr), ���������(hdr) � ����(msg) ������

	// << ������ ������� � ����������� ������ �������.

	if (xml.FindChildElem(XMLTAG_PARAMS))
	{
		xml.IntoElem();
		if (!xml.FindChildElem(XMLTAG_MAILER) || (mlr = xml.GetChildData()).size() <= 0)
			errors.push_back(ERR_NOT_CONTAIN_MAILER);
		if (!xml.FindChildElem(XMLTAG_SUBJECT) || (hdr = xml.GetChildData()).size() <= 0)
			errors.push_back(ERR_NOT_CONTAIN_HEADER);
		if (!xml.FindChildElem(XMLTAG_MSG) || (msg = xml.GetChildData()).size() <= 0)
			errors.push_back(ERR_NOT_CONTAIN_BODY);
	}
	else errors.push_back(ERR_NOT_CONTAIN_PARAMS);

	// << �������� ������.

	if (errors.size() == 0)
	{
		msg = 
			"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">" \
			"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1251\"" \
			"<html><head>" \
			"<title>" + hdr + "</title>" \
			"</head>" \
			"<body>" + msg + "</body>" \
			"</html>";

		if (!sys::SmtpClient::TrySendMail(SMTP_SERVER, mlr, ADMIN_EMAIL, hdr, msg))
			errors.push_back(ERR_NOT_SEND_EMAIL);
	}

	// << ������������ ������.

	xml.SetDoc("");
	xml.AddElem(XMLTAG_RESPONSE);
	xml.AddChildElem(XMLTAG_SERVICE, POST_SERV_NAME);
	if (errors.size() == 0)
	{
		xml.AddChildElem(XMLTAG_RESULT);
		xml.IntoElem();
		xml.AddChildElem(XMLTAG_MSG, INF_SUCCESS_SENT);
	}
	else // ������ ��� ������������ ������
	{
		xml.AddChildElem(XMLTAG_ERRORS);
		xml.IntoElem();
		for (unsigned int i = 0; i < errors.size(); ++i)
			xml.AddChildElem(XMLTAG_ERROR, errors[i]);
	}

	// << ���������� ��������� ������.

	::memset(outBuf->buf, 0, outBufSize);
	::strcpy_s(outBuf->buf, outBufSize, xml.GetDoc().c_str());
	outBuf->len = xml.GetDoc().length();

	return TRUE;
}