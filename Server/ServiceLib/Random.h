#pragma once

class Random
{
public:
	Random()
	{
	}
	Random(unsigned int seed)
	{
		::srand(seed);
	}
	int NextInt(int minValue, int maxValue) const
	{
		return (int)NextDouble((double)minValue, (double)maxValue);
	}
	double NextDouble(double minValue, double maxValue) const
	{
		if (minValue > maxValue)
			std::swap(minValue, maxValue);
		return 
			((double)::rand() / (RAND_MAX + 1) * (maxValue - minValue) + minValue);
	}
};