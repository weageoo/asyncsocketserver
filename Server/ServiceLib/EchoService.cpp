#include "EchoService.h"

BOOL EchoService(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize)
{
	CMarkup xml(inBuf); // ������� XML
	std::string msg;

	// << ������ �������.

	if (xml.FindChildElem(XMLTAG_PARAMS))
	{
		xml.IntoElem();

		if (xml.FindChildElem(XMLTAG_MSG))
			msg = xml.GetChildData();
	}

	// << ������������ ������.

	xml.SetDoc("");
	xml.AddElem(XMLTAG_RESPONSE);
	xml.AddChildElem(XMLTAG_SERVICE, ECHO_SERV_NAME);
	xml.AddChildElem(XMLTAG_RESULT);
	xml.IntoElem();
	xml.AddChildElem(XMLTAG_MSG, msg);

	// << �������� �������������� ����� � �����.

	::memset(outBuf->buf, 0, outBufSize);
	::strcpy_s(outBuf->buf, outBufSize, xml.GetDoc().c_str());
	outBuf->len = xml.GetDoc().length();

	return TRUE;
}