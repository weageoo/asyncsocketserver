#include "Support.h"
#include "ServicesTableMacro.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#include "EchoService.h"
#include "TimeService.h"
#include "RandService.h"
#include "PostService.h"
#include "QuitService.h"
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// �������, �������������� ���������-����������� �������� �� ������������ ��������-����������������.
BEGIN_TABLESERVICE
	ENTRYSERVICE("echo", EchoService),
	ENTRYSERVICE("time", TimeService),
	ENTRYSERVICE("rand", RandService),
	ENTRYSERVICE("post", PostService),
	ENTRYSERVICE("quit", QuitService)
END_TABLESERVICE;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// �������� ��������� �� � ��������������.
inline PSERVICE GetServiceProc(IN std::string serviceName)
{
	for (int i = TABLESERVICE_SZ; --i >= 0; )
		if (serviceName == TABLESERVICE_ID(i))
			return TABLESERVICE_FN(i);
	return NULL;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ���������� �������-���������� �� ����������� �������.
// �� ������ ������� ��������� ������������ ��������������.
// ���������� FALSE, ���� ������� ����� ���������.
EXTERN_C __declspec(dllexport) BOOL ProcessRequest(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BOOL APIENTRY DllMain(HMODULE, DWORD, LPVOID)
{
	return TRUE;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
EXTERN_C __declspec(dllexport)
BOOL ProcessRequest(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize)
{
	CMarkup xml(inBuf);

	if (xml.FindChildElem(XMLTAG_SERVICE))
		if (PSERVICE proc = GetServiceProc(xml.GetChildData()))
			return proc(inBuf, outBuf, outBufSize);

	// << ������ �� ���������. ��������� ��������� �� ������.

	xml.SetDoc("");
	xml.AddElem(XMLTAG_RESPONSE);
	xml.AddChildElem(XMLTAG_ERRORS);
	xml.IntoElem();
	xml.AddChildElem(XMLTAG_ERROR, ERR_NOT_SUPPORT_SERV);

	::memset(outBuf->buf, 0, outBufSize);
	::strcpy_s(outBuf->buf, outBufSize, xml.GetDoc().c_str());
	outBuf->len = xml.GetDoc().length();

	return TRUE;
}