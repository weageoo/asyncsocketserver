// ��� �������-������� [Name]Service(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize)
// inBuf - ������� �����
// outBuf - �������� �����
// outBufSize - ������ outBuf->buf
// ������� ������� ���������� � outBuf->len ���-�� �������� � outBuf->buf

RandService

��� �������

<REQUEST>
	<SERVICE>rand</SERVICE>
	<PARAMS>
		<MIN>-12</MIN> <!-- �� ���������� --> <!-- �� INT_MIN / 2 --> 
		<MAX>256</MAX> <!-- �� ���������� --> <!-- �� INT_MAX / 2 --> 
	</PARAMS>
</REQUEST>

��� ������

<RESPONSE>
	<SERVICE>rand</SERVICE>
	<RESULT>
		<NUM>123</NUM>
	</RESULT> <!-- �����������, ���� ������ �� ������ -->
</RESPONSE>

���

<RESPONSE>
	<SERVICE>rand</SERVICE>
	<ERRORS>
		<ERROR>Error message 1</ERROR>
		<ERROR>Error message 2</ERROR>
	</ERRORS> <!-- �����������, ���� ������ ������ -->
</RESPONSE>

------------------------------------------------------------------------------------------------------------------------

TimeService

��� �������

<REQUEST>
	<SERVICE>time</SERVICE>
	<PARAMS>
		<TIMEZONE>+2</TIMEZONE> <!-- �� ��������, � ���� ���� ��������� ����� -->
	</PARAMS>
</REQUEST>

��� ������

<RESPONSE>
	<SERVICE>rand</SERVICE>
	<RESULT>
		<YEAR>2009</YEAR>
		<MONTH>11</MONTH>	
		<DAY_OF_WEEK>7</DAY_OF_WEEK> <!-- �� 1 (�����������) �� 7 (�����������) -->
		<DAY>8</DAY>
		<HOUR>9</HOUR>
		<MINUTE>16</MINUTE>
		<SECOND>53</SECOND>
		<TIMEZONE>+120</TIMEZONE> <!-- �������� UTC, � ������� -->
	</RESULT>
</RESPONSE>

���

<RESPONSE>
	<SERVICE>time</SERVICE>
	<ERRORS>
		<ERROR>Error message 1</ERROR>
		<ERROR>Error message 2</ERROR>
	</ERRORS> <!-- �����������, ���� ������ ������ -->
</RESPONSE>

------------------------------------------------------------------------------------------------------------------------

PostService

��� �������

<REQUEST>
	<SERVICE>post</SERVICE>
	<PARAMS>
		<MAILER>user@mail.ru</MAILER>
		<SUBJECT>Test</SUBJECT>
		<MSG>Msg for Admin.</MSG>
	</PARAMS>
</REQUEST>

��� ������

<RESPONSE>
	<SERVICE>post</SERVICE>
	<RESULT>
		<MSG>The message is successfully sent.</MSG>
	</RESULT>
</RESPONSE>

���

<RESPONSE>
	<SERVICE>post</SERVICE>
	<ERRORS>
		<ERROR>Error message 1</ERROR>
		<ERROR>Error message 2</ERROR>
	</ERRORS> <!-- �����������, ���� ������ ������ -->
</RESPONSE>