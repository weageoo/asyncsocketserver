#include "RandService.h"

BOOL RandService(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize)
{
	static Random random((unsigned)::time(NULL));

	int mn = INT_MIN / 2, mx = INT_MAX / 2;
	CMarkup xml; // ������� XML

	// << ������ �������.

	xml.SetDoc(inBuf); 
	if (xml.FindChildElem(XMLTAG_PARAMS))
	{
		xml.IntoElem();
		if (xml.FindChildElem(XMLTAG_MIN))
			mn = sys::Convert::ToInt32(xml.GetChildData());		
		if (xml.FindChildElem(XMLTAG_MAX))
			mx = sys::Convert::ToInt32(xml.GetChildData());
	}

	// << ������������ ������.

	xml.SetDoc("");
	xml.AddElem(XMLTAG_RESPONSE);
	xml.AddChildElem(XMLTAG_SERVICE, RAND_SERV_NAME);
	xml.AddChildElem(XMLTAG_RESULT);
	xml.IntoElem();
	xml.AddChildElem(XMLTAG_NUM, random.NextInt(mn, mx));

	// << ���������� ��������� ������.

	::memset(outBuf->buf, 0, outBufSize);
	::strcpy_s(outBuf->buf, outBufSize, xml.GetDoc().c_str());
	outBuf->len = xml.GetDoc().length();

	return TRUE;
}