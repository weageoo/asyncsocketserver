#pragma once
#include "Support.h"

// ��� ������� - �������
typedef BOOL (*PSERVICE)(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize);

struct __TableEntry 
{
	std::string __Id;  
	PSERVICE    __Fn;
};

#define BEGIN_TABLESERVICE __TableEntry __TableService[] = {
#define ENTRYSERVICE(servName, servFun) { (servName), (servFun) }  
#define END_TABLESERVICE };
#define TABLESERVICE_ID(i) __TableService[i].__Id
#define TABLESERVICE_FN(i) __TableService[i].__Fn
#define TABLESERVICE_SZ sizeof(__TableService)/sizeof(__TableEntry)