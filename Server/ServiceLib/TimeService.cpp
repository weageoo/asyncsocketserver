#include "TimeService.h"

BOOL TimeService(LPCSTR inBuf, LPWSABUF outBuf, UINT outBufSize)
{
	SYSTEMTIME time, uTime;
	TIME_ZONE_INFORMATION tzInfo;
	CMarkup xml; // ������� XML

	// ������� ������� ����
	::GetTimeZoneInformation(&tzInfo);

	// << ������ �������.

	xml.SetDoc(inBuf);
	if (xml.FindChildElem(XMLTAG_PARAMS))
	{
		xml.IntoElem();
		if (xml.FindChildElem(XMLTAG_TIMEZONE))
			tzInfo.Bias = -(sys::Convert::ToInt32(xml.GetChildData()));
	}

	// << ������������ ������.

	xml.SetDoc("");
	xml.AddElem(XMLTAG_RESPONSE);
	xml.AddChildElem(XMLTAG_SERVICE, TIME_SERV_NAME);
	xml.AddChildElem(XMLTAG_RESULT);
	xml.IntoElem();

	// <!-- ���������� ���� � �������.

	::GetSystemTime(&time);
	::SystemTimeToTzSpecificLocalTime(&tzInfo, &time, &uTime);

	// -->

	xml.AddChildElem(XMLTAG_YEAR, uTime.wYear);
	xml.AddChildElem(XMLTAG_MONTH, uTime.wMonth);
	xml.AddChildElem(XMLTAG_DAY_OF_WEEK, uTime.wDayOfWeek);
	xml.AddChildElem(XMLTAG_DAY, uTime.wDay);
	xml.AddChildElem(XMLTAG_HOUR, uTime.wHour);
	xml.AddChildElem(XMLTAG_MINUTE, uTime.wMinute);
	xml.AddChildElem(XMLTAG_SECOND, uTime.wSecond);
	xml.AddChildElem(XMLTAG_TIMEZONE, -tzInfo.Bias);

	// << ����������� ������ � �������� �����

	::memset(outBuf->buf, 0, outBufSize);
	::strcpy_s(outBuf->buf, outBufSize, xml.GetDoc().c_str());
	outBuf->len = xml.GetDoc().length();

	return TRUE;
}